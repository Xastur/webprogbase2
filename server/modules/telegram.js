const config = require('../config/bot');

const TelegramBotApi = require('telegram-bot-api');

const User = require('../models/user.model');

let tokenRead = false;

const telegramBotApi = new TelegramBotApi({
    token: config.bot_token,
    updates: {
        enabled: true // do message pull
    }
});

telegramBotApi.on('message', onMessage);

async function registerTgUser(chatId) {
    const user = await User.getByChatId(chatId);
    if (!user) {
        return 'undef';
    }
    return 'def';
}

async function deleteTgUser(chatId) {
    const user = await User.getByChatId(chatId);
    if (user) {
        await User.update(user._id, { 'chatId': null });
        return 'def';
    }
    else
        return 'undef';
}

function onMessage(message) {
    processRequest(message)
        .catch(err => telegramBotApi.sendMessage({
            chat_id: message.chat.id,
            text: `Something went wrong. Try again later. Error: ${err.toString()}`,
        }));
}

async function processRequest(message) {
    const chatId = message.chat.id;
    if (message.text === '/start') {
        const res = await registerTgUser(chatId);
        if (res === 'def') {
            return telegramBotApi.sendMessage({
                chat_id: chatId,
                text: "You are already registered",
            });
        }
        else {
            tokenRead = true;
            return telegramBotApi.sendMessage({
                chat_id: chatId,
                text: "Please, type your tg token",
            });
        }
    }
    else if (message.text === '/runaway') {
        const res = await deleteTgUser(chatId);
        if (res === 'undef') {
            return telegramBotApi.sendMessage({
                chat_id: chatId,
                text: "You are not in the list",
            });
        }
        else {
            return telegramBotApi.sendMessage({
                chat_id: chatId,
                text: "Done",
            });
        }
    }
    else if (message.text === '/me') {
        const user = await User.getByChatId(chatId);

        if (!user) {
            return telegramBotApi.sendMessage({
                chat_id: chatId,
                text: "You are not in the list",
            });
        }
        else {
            const text = "Fullname: " + user.fullname + "\nLogin " + user.login + "\nE-mail: " + user.email;

            return telegramBotApi.sendMessage({
                chat_id: chatId,
                text: text,
            });
        }
    }
    else {
        if (tokenRead) {
            tokenRead = false;
            const token = message.text;

            const user = await User.findOne({ 'tgToken': token });
    
            if (user) {
    
                await User.update(user._id, { 'chatId': chatId });
    
                return telegramBotApi.sendMessage({
                    chat_id: chatId,
                    text: "Done",
                });
            }
            else {
                return telegramBotApi.sendMessage({
                    chat_id: chatId,
                    text: "Wrong token",
                });
            }
        }
        else {
            return telegramBotApi.sendMessage({
                chat_id: chatId,
                text: "Sorry, I did't get it",
            });
        }
    }
}

module.exports = {
    async sendNotification(text, id) {
        const user = await User.getById(id);

        console.log(`Notify ${user.subscribersId.length} users`);

        for (let i = 0; i < user.subscribersId.length; i++) {
            const subUser = await User.getById(user.subscribersId[i]);
            if (subUser.chatId) {
                await telegramBotApi.sendMessage({
                    chat_id: subUser.chatId,
                    text: text,
                });
            }
        }
    }
};