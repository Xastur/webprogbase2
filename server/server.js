const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const dbconfig = require('./config/database');
const JWTconfig = require('./config/jwt');
const usersRouter = require('./routes/users');
const blogsRouter = require('./routes/blogs');
const authRouter = require('./routes/auth');
const companiesRouter = require('./routes/companies');
const searchRouter = require('./routes/search');
const commentsRouter = require('./routes/comments');
const botRouter = require('./routes/bot');
const path = require('path');
const bodyParser     = require('body-parser');
const cookieParser = require('cookie-parser');

require('dotenv').config();

const app = express();

app.use(express.static(path.join(__dirname, 'build')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser());
app.use(cors());
app.use(express.json());
app.use(cookieParser());
app.use(cookieParser(JWTconfig.jwt.secret));

const passport = require('./config/passport');

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static('client'));

app.use('/users', usersRouter);
app.use('/blogs', blogsRouter);
app.use('/companies', companiesRouter);
app.use('/auth', authRouter);
app.use('/search', searchRouter);
app.use('/comments', commentsRouter);
app.use('/bot', botRouter);

const databaseUrl = dbconfig.DatabaseUrl;
const serverPort = dbconfig.ServerPort;
const conOptions = { useNewUrlParser: true }

mongoose.connect(databaseUrl, conOptions)
    .then(() => console.log(`Database connected: ${databaseUrl}`))
    .then(() => app.listen(serverPort, () => console.log(`Listening on port ${serverPort}`)))
    .catch(err => console.log(`Error: ${err}`))