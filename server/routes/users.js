const router = require('express').Router();
let User = require('../models/user.model');
const passport = require("../config/passport");
const dbconfig = require('../config/database');
const cloudinary = require('cloudinary');
const pmongo = require('promised-mongo');
const ObjectId = pmongo.ObjectId;
let multer = require("multer");

let storage = multer.diskStorage({})
let upload = multer({ storage: storage })

cloudinary.config({
    cloud_name: dbconfig.cloud_name,
    api_key: dbconfig.api_key,
    api_secret: dbconfig.api_secret
});

router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    User.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).json('Error: ' + err));
})

router.get('/me', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.status(200).send({ user: req.user });
})

router.get('/token/:id', (req, res) => {
    const resetToken = req.params.id;

    User.findOne({ 'resetPasswordToken': resetToken })
        .then(user => res.status(200).send(user))
        .catch(err => res.status(404).send(err))
})

router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const userId = req.params.id;

    User.findById(userId)
        .then(user => res.status(200).json(user))
        .catch(err => {
            res.status(404).send(err.message)
        })
})

router.put('/update/subBlogsId/add', passport.authenticate('jwt', { session: false }), upload.single("image"), (req, res) => {
    let subscribersId = req.user.subscribersId;

    User.updateMany({ '_id': { $in: subscribersId } }, { $push: { 'subBlogsId': ObjectId(req.body.blogId) } })
        .then(x => res.status(200).send("Added"))
        .catch(err => res.status(404).send(err))
})

router.put('/update/subBlogsId/delete', passport.authenticate('jwt', { session: false }), upload.single("image"), (req, res) => {
    // let subscribersId = req.body.subscribersId;

    // for (let i = 0; i < subscribersId.length; i++) {
    //     subscribersId[i] = ObjectId(subscribersId[i]);
    // }
    User.updateMany({}, { $pull: { 'subBlogsId': ObjectId(req.body.blogId) } })
        .then(x => res.status(200).send("Deleted"))
        .catch(err => res.status(404).send(err))
})  

router.put('/:id/update', passport.authenticate('jwt', { session: false }), upload.single("image"), (req, res) => {
    const userId = req.params.id;

    let fullname;
    let biography;
    let companiesId;
    let subscribersId;
    let subscriptionsId;
    let subBlogsId;

    User.getById(userId)
        .then(user => {
            fullname = user.fullname;
            biography = user.biography;
            companiesId = user.companiesId;
            subscribersId = user.subscribersId;
            subscriptionsId = user.subscriptionsId;
            subBlogsId = user.subBlogsId;

            if (req.body.fullname)
                fullname = req.body.fullname;
            if (req.body.biography)
                biography = req.body.biography;
            if (req.body.companyId) {
                if (companiesId.length) {
                    for (let i = 0; i < companiesId.length; i++) {
                        if (companiesId[i].toString() === req.body.companyId) {
                            companiesId.splice(companiesId.indexOf(ObjectId(req.body.companyId)), 1);
                            break;
                        }
                        if (i === companiesId.length - 1) {
                            companiesId = companiesId.concat(ObjectId(req.body.companyId));
                            break;
                        }
                    }
                }
                else
                    companiesId = companiesId.concat(ObjectId(req.body.companyId));
            }
            if (req.body.subscriberId) {
                if (subscribersId.length) {
                    for (let i = 0; i < subscribersId.length; i++) {
                        if (subscribersId[i].toString() === req.body.subscriberId) {
                            subscribersId.splice(subscribersId.indexOf(ObjectId(req.body.subscriberId)), 1);
                            break;
                        }
                        if (i === subscribersId.length - 1) {
                            subscribersId = subscribersId.concat(ObjectId(req.body.subscriberId));
                            break;
                        }
                    }
                }
                else {
                    subscribersId = subscribersId.concat(ObjectId(req.body.subscriberId));
                }
            }
            if (req.body.subscriptionId) {
                if (subscriptionsId.length) {
                    for (let i = 0; i < subscriptionsId.length; i++) {
                        if (subscriptionsId[i].toString() === req.body.subscriptionId) {
                            subscriptionsId.splice(subscriptionsId.indexOf(ObjectId(req.body.subscriptionId)), 1);
                            break;
                        }
                        if (i === subscriptionsId.length - 1) {
                            subscriptionsId = subscriptionsId.concat(ObjectId(req.body.subscriptionId));
                            break;
                        }
                    }
                }
                else
                    subscriptionsId = subscriptionsId.concat(ObjectId(req.body.subscriptionId));
            }
            if (req.body.subBlogId) {
                subBlogsId.splice(subBlogsId.indexOf(ObjectId(req.body.subBlogId)), 1);
            }

            if (req.file) 
                return cloudinary.v2.uploader.upload(req.file.path);
            else {
                const updateInfo = {
                    'fullname': fullname,
                    'biography': biography,
                    'companiesId': companiesId,
                    'subscriptionsId': subscriptionsId,
                    'subscribersId': subscribersId,
                    'subBlogsId': subBlogsId
                }
                return User.update(userId, updateInfo)
            }
        })
        .then(x => {
            if (x.url) {
                const updateInfo = {
                    'fullname': fullname,
                    'biography': biography,
                    'companiesId': companiesId,
                    'subscriptionsId': subscriptionsId,
                    'subscribersId': subscribersId,
                    'subBlogsId': subBlogsId,
                    'avaUrl': x.url
                }
                return User.update(userId, updateInfo)
            }
        })
        .then(x => res.status(200).send({ message: "Updated" }))
        .catch(err => res.status(404).send(err.message))
})

router.delete('/:id/delete', passport.authenticate('jwt', { session: false }), (req, res) => {
    const userId = req.params.id;

    User.update(userId, { 'isDisabled': true })
        .then(x => res.status(200).send("Deleted"))
        .catch(err => res.status(404).send(err))
})

module.exports = router;