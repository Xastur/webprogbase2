const router = require('express').Router();
let User = require('../models/user.model');
const passport = require('../config/passport');
const Telegram = require('../modules/telegram');
let multer = require("multer");

let storage = multer.diskStorage({})
let upload = multer({ storage: storage })

router.post('/', upload.single("image"), async (req, res) => {
    const id = req.body.id;
    const artTitle = req.body.artTitle;
    const url = req.body.url;
    const login = req.body.userLogin;
    let resp = "-----New Blog From " + login + "-----\n" + artTitle + '\n' + url; 
    try {
        await Telegram.sendNotification(resp, id);
        res.status(200).send("Sent");
    } catch (err) {
        res.send(err.toString());
    }
});

module.exports = router;