const router = require('express').Router();
let Company = require('../models/company.model');
let Comment = require('../models/comment.model');
const passport = require("../config/passport");
const dbconfig = require('../config/database');
const mongoose = require('mongoose');
let multer = require("multer");
const cloudinary = require('cloudinary');
let User = require('../models/user.model');
let Blog = require('../models/blog.model');
const pmongo = require('promised-mongo');
const ObjectId = pmongo.ObjectId;

let storage = multer.diskStorage({})
let upload = multer({ storage: storage })

cloudinary.config({
    cloud_name: dbconfig.cloud_name,
    api_key: dbconfig.api_key,
    api_secret: dbconfig.api_secret
});

router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {

    Company.getAll()
        .then(companies => res.json(companies))
        .catch(err => res.status(400).json('Error: ' + err));
})

router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const companyId = req.params.id;

    Company.getCompany(companyId)
        .then(company => res.json(company))
        .catch(err => res.status(404).json('Error: ' + err));
})

router.put('/:id/update', passport.authenticate('jwt', { session: false }), upload.single("image"), (req, res) => {
    const companyId = req.params.id;

    let companyName;
    let aboutCompany;
    let admins;

    Company.getCompany(companyId)
        .then(company => {
            companyName = company.companyName;
            aboutCompany = company.aboutCompany;
            admins = company.admins;

            if (req.body.companyName)
                companyName = req.body.companyName;
            if (req.body.aboutCompany)
                aboutCompany = req.body.aboutCompany;
            if (req.body.admin) {

                if (admins.length) {
                    for (let i = 0; i < admins.length; i++) {
                        if (admins[i].toString() === req.body.admin) {
                            admins.splice(admins.indexOf(ObjectId(req.body.admin)), 1);
                            break;
                        }
                        if (i === admins.length - 1) {
                            admins = admins.concat(ObjectId(req.body.admin));
                            break;
                        }
                    }
                }
                else
                    admins = admins.concat(ObjectId(req.body.admin));
            }

            if (req.file)
                return cloudinary.v2.uploader.upload(req.file.path);
            else {
                const updateInfo = {
                    'companyName': companyName,
                    'aboutCompany': aboutCompany,
                    'admins': admins
                }
                return Company.update(companyId, updateInfo);
            }

        })
        .then(x => {
            if (x.url) {
                const updateInfo = {
                    'companyName': companyName,
                    'aboutCompany': aboutCompany,
                    'admins': admins,
                    'imageSrc': x.url
                }
                return Company.update(companyId, updateInfo);
            }
        })
        .then(x => {
            res.status(200).send({ message: "Updated" });
        })
        .catch(err => {
            res.status(404).send(err.message);
        })
})

router.post('/create', passport.authenticate('jwt', { session: false }), upload.single("image"), (req, res) => {
    const date = new Date();

    if (req.file) {

        let newCompany = new Company();
        newCompany._id = new mongoose.Types.ObjectId();
        newCompany.companyName = req.body.companyName;
        newCompany.admins = req.user._id;
        newCompany.articlesId = [];
        newCompany.aboutCompany = req.body.aboutCompany;
        newCompany.date = date;


        cloudinary.v2.uploader.upload(req.file.path, function (err, data) {
            if (err) {
                res.status(400).send(err);
                return;
            }

            newCompany.imageSrc = data.url;

            Company.insert(newCompany)
                .then(x => {
                    return User.update(req.user._id, { $push: { 'companiesId': x._id } })
                })
                .then(x => res.status(201).json(newCompany))
                .catch(err => res.status(404).send(err))
        })
    }
    else
        res.status(400).send("Photo required");
})

router.delete('/:id/delete', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const companyId = req.params.id;
    let company_;
    let commentsId;

    let company = await Company.getCompany(companyId);

    for (let i = 0; i < company.articlesId.length; i++) {
        let blog = await Blog.delete(company.articlesId[i]);
        await Comment.deleteMany({ '_id': { $in: blog.commentsId } });
    }

    Company.getCompany(companyId)
        .then(company => {
            company_ = company;
            // return Blog.deleteMany(company.articlesId);
            return User.updateMany({ 'subBlogsId': { $in: company_.articlesId } }, { $pull: { 'subBlogsId': { $in: company_.articlesId } } });
        })
        .then(x => {
            // console.log("X:", x);
            // return User.updateMany({ 'subBlogsId': { $in: company_.articlesId } }, { $pull: { 'subBlogsId': { $in: company_.articlesId } } });
            return User.updateMany({ $pull: { 'companiesId': company_._id } })
        })
        // .then(x => {
        //     return User.updateMany({ $pull: { 'companiesId': company_._id } })
        // })
        .then(x => {
            return Company.delete(companyId);
        })
        .then(x => {
            res.status(202).send("Deleted");
        })
        .catch(err => res.status(404).send(err))
})

module.exports = router;