const router = require('express').Router();
let User = require('../models/user.model');
let Comment = require('../models/comment.model');
let Blog = require('../models/blog.model');
const passport = require('../config/passport');
const mongoose = require('mongoose');
let multer = require("multer");

let storage = multer.diskStorage({})
let upload = multer({ storage: storage })

router.post('/create', passport.authenticate('jwt', { session: false }), upload.single("image"), (req, res) => {
    const date = new Date();

    let newComment = new Comment();

    newComment._id = new mongoose.Types.ObjectId();
    // newComment.userLogin = req.user.login;
    newComment.userId = req.user._id;
    newComment.comment = req.body.comment;
    // newComment.userAvaUrl = req.user.avaUrl;
    newComment.createdAt = date;

    Comment.insert(newComment)
        .then(x => {
            const blogId = req.body._id;
            return Blog.update(blogId, { $push: { 'commentsId': x._id } });
        })
        .then(x => res.status(201).send())
        .catch(err => res.status(404).send(err))
})

router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    Comment.find()
        .then(comments => res.status(200).send(comments))
        .catch(err => res.status(400).send(err))
})

router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const commentId = req.params.id;
    
    Comment.findOne({ '_id': commentId })
        .then(comments => res.status(200).send(comments))
        .catch(err => res.status(404).send(err))
})

router.delete('/:id/delete', passport.authenticate('jwt', { session: false }), (req, res) => {
    const commentId = req.params.id;

    Comment.delete(commentId)
        .then(x => res.status(200).send("Deleted"))
        .catch(err => res.status(404).send(err))
})

module.exports = router;