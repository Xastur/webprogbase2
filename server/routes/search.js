const router = require('express').Router();
const jwt = require('jsonwebtoken');
const passport = require("../config/passport");
const User = require('../models/user.model');
const Blog = require('../models/blog.model');
const Company = require('../models/company.model');

router.get('/:req', passport.authenticate('jwt', { session: false }), (req, res) => {
    const request = req.params.req;

    let p1 = User.getAll();
    let p2 = Blog.getAll();
    let p3 = Company.getAll();

    Promise.all([p1, p2, p3])
        .then(([users, blogs, companies]) => {
            let resArray = new Array();

            let length;
            if (users.length > blogs.length && users.length > companies.length)
                length = users.length;
            else if (blogs.length > users.length && blogs.length > companies.length)
                length = blogs.length;
            else
                length = companies.length;

            for (let i = 0; i < length; i++) {
                if (i < users.length)
                    if ((users[i].login.toLowerCase().indexOf(request.toLowerCase()) > -1 || users[i].fullname.toLowerCase().indexOf(request.toLowerCase()) > -1) && !users[i].isDisabled)
                        resArray.push(users[i]);
                if (i < blogs.length)
                    if (blogs[i].artTitle.toLowerCase().indexOf(request.toLowerCase()) > -1 && blogs.length > i)
                        resArray.push(blogs[i]);
                if (i < companies.length)
                    if (companies[i].companyName.toLowerCase().indexOf(request.toLowerCase()) > -1 && companies.length > i)
                        resArray.push(companies[i]);
            }
            res.status(200).send(resArray);
        })
        .catch(err => {
            res.status(400).send(err)
        })
})

module.exports = router;