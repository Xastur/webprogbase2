const router = require('express').Router();
const jwt = require('jsonwebtoken');
const passport = require("../config/passport");
let User = require('../models/user.model');
const mailacc = require('../config/mailaccount');
const nodemailer = require("nodemailer");
const url = require('../config/url');
var bcrypt = require('bcrypt-nodejs');
let multer = require("multer");

let storage = multer.diskStorage({})
let upload = multer({ storage: storage })

router.post('/login', (req, res) => {
    passport.authenticate('local', { session: false }, (err, user, info) => {
        if (err || !user) {
            return res.status(404).json({
                message: `Something is not right: ${JSON.stringify(info)}`,
                user: user
            });
        }
        req.login(user, { session: false }, (err) => {
            if (err) {
                res.status(400).send(err);
                return;
            }
            const token = jwt.sign(JSON.stringify(user), 'your_jwt_secret');
            return res.json({ user, token });
        });
    })(req, res);
})

router.post('/signup', (req, res) => {
    passport.authenticate('local-signup', (err, user, info) => {
        if (err) {
            res.status(400).send(err)
            return;
        }
        else if (info) {
            res.send(info);
            return;
        }
        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: mailacc.user,
                pass: mailacc.pass
            }
        })

        let mes = transporter.sendMail({
            from: '"Out Of Lag"' + mailacc.user, // sender address
            to: user.email, // list of receivers
            subject: "[OutOfLag] Activate Your Account", // Subject line
            text: "",
            html: "<p>Click the link below to activate your OutOfLag account:</p><a href=" + url.url + "/auth/activate/" + user.activateToken + ">Click</a><p>Your Telegram token: " + user.tgToken + "</p><p>If you did not request an account creation you can safely ignore this email.</p>"
        });

        mes
            .then(x => {
                return res.status(200).send("Sent");
            })
            .catch(err => {
                return res.status(400).send(err);
            })

    })(req, res);
})

router.post('/passwordreset', (req, res) => {

    const token = Math.random().toString(36).substring(2, 16);
    let options = {
        resetPasswordToken: token,
        resetPasswordExpires: Date.now() + 3600000 // 1 hour
    }
    User.updateOne({ email: req.body.email }, options)
        .then(x => {
            if (x.n) {
                let transporter = nodemailer.createTransport({
                    service: 'Gmail',
                    auth: {
                        user: mailacc.user,
                        pass: mailacc.pass
                    }
                })
                let info = transporter.sendMail({
                    from: '"Out Of Lag"' + mailacc.user, // sender address
                    to: req.body.email, // list of receivers
                    subject: "[OutOfLag] Password Reset E-mail", // Subject line
                    text: "You're receiving this e-mail because you or someone else has requested a password reset for your user account.\nClick the link below to reset your password:\n----\nIf you did not request a password reset you can safely ignore this email.", // plain text body
                    html: "<p>You're receiving this e-mail because you or someone else has requested a password reset for your user account.</p><p>Click the link below to reset your password:</p><a href=" + url.clientUrl + "/password/change/" + token + ">Click</a><p>If you did not request a password reset you can safely ignore this email.</p>" // html body
                });
    
                return info;
            }
            else {
                res.status(404).send("Wrong E-mail");
            }
        })
        .then(info => {
            res.status(200).send(info.messageId);
        })
        .catch(err => {
            res.status(404).send(err);
        })
})

router.post('/password/change/:token', upload.single("image"), (req, res) => {

    const resetToken = req.params.token;
    const newPassword = req.body.password;

    if (resetToken) {
        User.findOne({ 'resetPasswordToken': resetToken })
            .then(user => {
                if (user) {

                    if (user.resetPasswordExpires > Date.now()) {
                        const updInfo = {
                            'resetPasswordToken': null,
                            'resetPasswordExpires': null,
                            'password': bcrypt.hashSync(newPassword, bcrypt.genSaltSync(8))
                        }
                        return User.update(user._id, updInfo);
                    }
                    else {
                        res.redirect(url.clientUrl + '/auth/passwordreset?error=Bad+Token');
                        return;
                    }
                }
                else {
                    res.redirect(url.clientUrl + '/auth/passwordreset?error=Bad+Token');
                    return;
                }
            })
            .then(user => {
                if (user) {
                    res.status(200).send(user);
                }
            })
            .catch(err => {
                res.redirect(url.clientUrl + '/auth/passwordreset?error=Bad+Token');
            })
    }
    else
        res.redirect(url.clientUrl + '/auth/passwordreset?error=Bad+Token');
})

router.get('/activate/:token', (req, res) => {
    const activateToken = req.params.token;
    User.updateOne({ 'activateToken': activateToken }, { 'isDisabled': false, 'activateToken': null })
        .then(x => {
            res.redirect(url.clientUrl + '/auth/login');
        })
        .catch(err => {
            res.status(404).send(err);
        })
})

module.exports = router;