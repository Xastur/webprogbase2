const router = require('express').Router();
let Blog = require('../models/blog.model');
let User = require('../models/user.model');
let Comment = require('../models/comment.model');
let Company = require('../models/company.model');
const passport = require('../config/passport');
const dbconfig = require('../config/database');
let multer = require("multer");
const cloudinary = require('cloudinary');
var mongoose = require('mongoose');
const pmongo = require('promised-mongo');
const ObjectId = pmongo.ObjectId;

let storage = multer.diskStorage({})
let upload = multer({ storage: storage })

cloudinary.config({
    cloud_name: dbconfig.cloud_name,
    api_key: dbconfig.api_key,
    api_secret: dbconfig.api_secret
});

router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    Blog.getAll()
        .then(blogs => res.json(blogs))
        .catch(err => res.status(400).send(err));
})

router.post('/create', passport.authenticate('jwt', { session: false }), upload.any(), async (req, res) => {

    let companyId = req.body.companyId;

    if (!req.files.length || req.files.length > 10) {
        res.status(400).send("Files amount err");
        return;
    }

    const date = new Date();

    let newBlog = new Blog();
    newBlog._id = new mongoose.Types.ObjectId();
    newBlog.artTitle = req.body.artTitle;
    newBlog.userId = req.user._id;
    newBlog.commentsId = [];
    newBlog.aboutMyCar = req.body.aboutMyCar;
    newBlog.passportData = req.body.passportData;
    newBlog.viewsId = [];
    newBlog.likesId = [];
    newBlog.viewsAmount = 0;
    newBlog.date = date;
    newBlog.companyId = null;

    if (companyId !== 'undefined') {
        newBlog.companyId = ObjectId(companyId);
    }

    let arr = new Array();
    for (let i = 0; i < req.files.length; i++) {
        let a = await cloudinary.v2.uploader.upload(req.files[i].path);
        arr.push(a.url);

        if (i === req.files.length - 1) {
            newBlog.imagesSrc = arr;
            Blog.insert(newBlog)
                .then(x => {
                    return User.update(req.user._id, { $push: { 'articlesId': x._id } })
                })
                .then(x => {
                    if (companyId !== 'undefined')
                        return Company.update(ObjectId(req.body.companyId), { $push: { 'articlesId': newBlog._id } });

                    return;
                })
                .then(x => res.status(201).json(newBlog))
                .catch(err => res.status(404).send(err))
        }
    }
})

router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const blogId = req.params.id;

    Blog.getArticle(blogId)
        .then(blog => res.json(blog))
        .catch(err => res.status(404).json('Error: ' + err));
})

router.put('/:id/update', passport.authenticate('jwt', { session: false }), upload.any(), async (req, res) => {
    const blogId = req.params.id;

    if (req.files.length) {
        if (!req.files.length || req.files.length > 10) {
            res.status(400).send("Files amount err");
            return;
        }

        let arr = new Array();
        for (let i = 0; i < req.files.length; i++) {
            let a = await cloudinary.v2.uploader.upload(req.files[i].path);
            arr.push(a.url);

            if (i === req.files.length - 1) {
                const updateInfo = {
                    'artTitle': req.body.artTitle,
                    'aboutMyCar': req.body.aboutMyCar,
                    'passportData': req.body.passportData,
                    'imagesSrc': arr,
                }

                Blog.update(blogId, updateInfo)
                    .then(x => res.status(200).send({ message: "Updated" }))
                    .catch(err => res.status(404).send(err))
            }
        }
    }
    else {
        const updateInfo = {
            'artTitle': req.body.artTitle,
            'aboutMyCar': req.body.aboutMyCar,
            'passportData': req.body.passportData,
        }

        Blog.update(blogId, updateInfo)
            .then(x => res.status(200).send({ message: "Updated" }))
            .catch(err => res.status(404).send(err))
    }
})

router.delete('/:id/delete', passport.authenticate('jwt', { session: false }), (req, res) => {
    const blogId = req.params.id;
    let commentsId;
    let companyId;

    Blog.delete(blogId)
        .then(x => {
            commentsId = x.commentsId;
            companyId = x.companyId;
            return User.update(x.userId, { $pull: { 'articlesId': x._id } })
        })
        .then(x => {
            if (commentsId)
                return Comment.deleteMany({ '_id': { $in: commentsId } });
            return;
        })
        .then(x => {
            if (companyId) {
                return Company.update(companyId, { $pull: { 'articlesId': ObjectId(blogId) } });
            }
        })
        .then(x => res.status(202).send("Deleted"))
        .catch(err => {
            res.status(404).send(err);
        })
})

router.put('/:id/deletecomment', passport.authenticate('jwt', { session: false }), upload.single("image"), (req, res) => {
    const blogId = req.params.id;

    Blog.update(blogId, { $pull: { 'commentsId': ObjectId(req.body.commentId) } })
        .then(x => {
            res.status(202).send("Updated");
        })
        .catch(err => {
            res.status(404).send(err);
        })
})

router.put('/:id/like', passport.authenticate('jwt', { session: false }), upload.single("image"), (req, res) => {
    const blogId = req.params.id;
    let likesId;

    Blog.getArticle(blogId)
        .then(blog => {
            likesId = blog.likesId;
            if (likesId.length) {

                for (let i = 0; i < likesId.length; i++) {
                    if (likesId[i].toString() === req.body.userId) {
                        likesId.splice(likesId.indexOf(ObjectId(req.body.userId)), 1);
                        break;
                    }
                    if (i === likesId.length - 1) {
                        likesId = likesId.concat(ObjectId(req.body.userId));
                        break;
                    }
                }
            }
            else
                likesId = likesId.concat(ObjectId(req.body.userId));

            return Blog.update(blogId, { 'likesId': likesId });
        })
        .then(x => {
            x.likesId = likesId;
            res.status(200).send(x);
        })
        .catch(err => res.status(404).send(err))
})

router.put('/:id/view', passport.authenticate('jwt', { session: false }), upload.single("image"), (req, res) => {
    const blogId = req.params.id;
    let viewsId;

    Blog.getArticle(blogId)
        .then(blog => {
            viewsId = blog.viewsId;
            if (viewsId.length) {
                for (let i = 0; i < viewsId.length; i++) {
                    if (viewsId[i].toString() === req.body.userId) {
                        break;
                    }
                    if (i === viewsId.length - 1) {
                        viewsId = viewsId.concat(ObjectId(req.body.userId));
                        break;
                    }
                }
            }
            else {
                viewsId = viewsId.concat(ObjectId(req.body.userId));
            }

            return Blog.update(blogId, { 'viewsId': viewsId });
        })
        .then(blog => {
            blog.viewsId = viewsId;
            res.status(200).send(blog);
        })
        .catch(err => res.status(404).send(err))
})

module.exports = router;