import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

const User = props => (
    <tr>
        <td><Link to={"/users/" + props.user._id} className="text-dark"><img height="64" src={props.user.avaUrl} alt="img" /></Link></td>
        <td><Link to={"/users/" + props.user._id} className="text-dark">{props.user.login}</Link></td>
        <td>{props.user.email}</td>
        <td>{props.user.fullname}</td>
    </tr>
)

export default class SubscribersList extends Component {
    constructor(props) {
        super(props);

        this.onPrev = this.onPrev.bind(this);
        this.onNext = this.onNext.bind(this);
        this.onLast = this.onLast.bind(this);
        this.onFirst = this.onFirst.bind(this);

        this.state = {
            users: [],
            usersId: [],
            currPage: 1,
            usersAmount: 4,
            lastPage: 1,
            loading: true
        };
    }

    componentDidMount() {
        document.title = "Subscribers";

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            this.setState({ isLoggedIn: true });
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };
            axios.get(url + '/users/' + this.props.match.params.id, reqOptions)
                .then(user => {
                    if (user.data) {
                        this.setState({ usersId: user.data.subscribersId });
                        return axios.get(url + '/users', reqOptions);
                    }
                    else {
                        this.props.history.push("/notfound");
                        return;
                    }
                })
                .then(users => {
                    if (users.data) {
                        for (let i = 0; i < users.data.length; i++) {
                            for (let j = 0; j < this.state.usersId.length; j++) {
                                if (users.data[i]._id === this.state.usersId[j]) {
                                    this.setState({ users: this.state.users.concat(users.data[i]) });
                                    break;
                                }
                            }
                        }
                        this.setState({ loading: false });
                    }
                })
                .catch(error => this.props.history.push("/notfound"))
        }
        else
            this.props.history.push("/unauthorized");
    }

    onPrev(e) {
        if (this.state.currPage > 1)
            this.setState({ currPage: this.state.currPage - 1 });
    }

    onNext(e) {
        if (this.state.currPage < this.state.lastPage)
            this.setState({ currPage: this.state.currPage + 1 });
    }

    onLast(e) {
        this.setState({ currPage: this.state.lastPage });
    }

    onFirst(e) {
        this.setState({ currPage: 1 });
    }

    UserList() {
        let currPage = this.state.currPage;
        let usersAmount = this.state.usersAmount;
        let users = this.state.users;
        let i = (currPage - 1) * usersAmount;

        let usersList = [];
        for (; i < (currPage - 1) * usersAmount + usersAmount && i < users.length; i++) {
            usersList.push(<User user={users[i]} key={users[i]._id} />);
        }
        return usersList;
    }

    render() {
        const firstPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const lastPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const elsePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const onePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )

        let pagination;

        if (this.state.lastPage === 1)
            pagination = onePagePagination;
        else if (this.state.currPage === 1)
            pagination = firstPagePagination;
        else if (this.state.currPage === this.state.lastPage)
            pagination = lastPagePagination;
        else
            pagination = elsePagePagination

        return (
            <div className="container">
                {this.state.loading ? "Loading..."
                    :
                    <>
                        {this.state.usersId.length ?
                            <>
                                <h3>Subscribers</h3>
                                <table className="table">
                                    <thead className="thead-light">
                                        <tr>
                                            <th></th>
                                            <th>Login</th>
                                            <th>Email</th>
                                            <th>Fullname</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.UserList()}
                                    </tbody>
                                </table>
                                <nav aria-label="pagination">
                                    {pagination}
                                </nav>
                            </>
                            : <h3>No Subscribers</h3>
                        }
                    </>
                }
            </div>
        )
    }
}