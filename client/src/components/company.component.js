import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;


export default class Company extends Component {

    constructor(props) {
        super(props);

        this.state = {
            _id: '',
            companyName: '',
            admins: [],
            articlesId: [],
            aboutCompany: '',
            date: '',
            imageSrc: '',
            owner: false,
            loading: true
        }
    }

    handleClick = e => {
        this.props.history.push("/companies/" + this.state._id + "/update");
    }

    componentDidMount() {

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            axios.get(url + window.location.pathname, reqOptions)
                .then(x => {
                    if (!x.data) {
                        this.props.history.push("/notfound");
                        return;
                    }
                    // console.log(x.data);
                    document.title = x.data.companyName;

                    this.setState({ _id: x.data._id });
                    this.setState({ companyName: x.data.companyName });
                    this.setState({ admins: x.data.admins });
                    this.setState({ articlesId: x.data.articlesId });
                    this.setState({ aboutCompany: x.data.aboutCompany });
                    this.setState({ date: x.data.date });
                    this.setState({ imageSrc: x.data.imageSrc });

                    return axios.get(url + '/users/me', reqOptions);
                })
                .then(user => {
                    if (user.data) {
                        if (user.data.user.role) {
                            this.setState({ owner: true });
                        }
                        else {
                            for (let i = 0; i < this.state.admins.length; i++) {
                                if (this.state.admins[i] === user.data.user._id) {
                                    this.setState({ owner: true });
                                }
                            }
                        }
                        this.setState({ loading: false });
                    }
                })
                .catch(err => {
                    this.props.history.push("/notfound");
                })
        }
        else
            this.props.history.push("/unauthorized");
    }

    render() {

        const updBtn = (
            <input type="submit"
                className="btn btn-dark"
                value="Settings"
                onClick={this.handleClick}
            />
        )
        const adminsBtn = (
            <input type="submit"
                className="btn btn-dark"
                value="Admins"
                id="myPageBtnsId"
            />
        )
        const companyBlogsBtn = (
            <input type="submit"
                className="btn btn-dark"
                value="Our Blogs"
                id="myPageBtnsId"
            />
        )
        const companyBlogsUrl = '/companies/' + this.state._id + '/blogs';
        const adminsUrl = '/companies/' + this.state._id + '/admins';

        return (
            <div className="container">
                {this.state.loading ? "Loading..."
                    :
                    <>
                        <img id="ava" src={this.state.imageSrc} alt="img" />
                        <h1>{this.state.companyName}</h1>
                        <hr />
                        {this.state.aboutCompany.length ?
                            <>
                                <h5>About company:</h5>
                                <p id="textStyle">{this.state.aboutCompany}</p>
                            </> : ""}

                        {this.state.owner ? updBtn : ''}
                        {/* <br /> */}
                        <Link to={adminsUrl}>
                            {adminsBtn}
                        </Link>
                        {/* <br /> */}
                        <Link to={companyBlogsUrl}>
                            {companyBlogsBtn}
                        </Link>
                    </>
                }
            </div>
        )
    }
}