import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

const User = props => (
    <tr>
        <td><Link to={"/users/" + props.user._id} className="text-dark"><img height="64" src={props.user.avaUrl} alt="img" /></Link></td>
        <td><Link to={"/users/" + props.user._id} className="text-dark">{props.user.login}</Link></td>
        <td>{props.user.email}</td>
        <td>{props.user.fullname}</td>
    </tr>
)

export default class CompanyAdmins extends Component {

    constructor(props) {
        super(props);

        this.onPrev = this.onPrev.bind(this);
        this.onNext = this.onNext.bind(this);
        this.onLast = this.onLast.bind(this);
        this.onFirst = this.onFirst.bind(this);

        this.state = {
            adminsId: [],
            admins: [],
            currPage: 1,
            usersAmount: 2,
            lastPage: 1,
            isAdmin: false
        }
    }

    componentDidMount() {
        document.title = "Admins";

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            }

            axios.get(url + '/companies/' + this.props.match.params.id, reqOptions)
                .then(company => {
                    if (!company.data) {
                        this.props.history.push("/notfound");
                        return;
                    }

                    this.setState({ adminsId: company.data.admins });

                    return axios.get(url + '/users', reqOptions);
                })
                .then(users => {
                    if (users.data) {
                        users.data.forEach(user => {
                            for (let i = 0; i < this.state.adminsId.length; i++) {
                                if (user._id === this.state.adminsId[i]) {
                                    this.setState({ admins: this.state.admins.concat(user) });
                                }
                            }
                        })
                        const lp = Math.ceil(parseInt(this.state.admins.length) / (this.state.usersAmount));
                        this.setState({ lastPage: lp });

                        return axios.get(url + '/users/me', reqOptions)
                    }
                    return;
                })
                .then(user => {
                    if (user.data) {
                        for (let i = 0; i < this.state.adminsId.length; i++) {
                            if (user.data.user._id === this.state.adminsId[i]) {
                                this.setState({ isAdmin: true });
                                // console.log("IS ADMIN");
                                break;
                            }
                        }
                    }
                })
                .catch(err => console.log(err))
        }
        else
            this.props.history.push("/unauthorized");
    };

    onPrev(e) {
        if (this.state.currPage > 1)
            this.setState({ currPage: this.state.currPage - 1 });
    }

    onNext(e) {
        if (this.state.currPage < this.state.lastPage)
            this.setState({ currPage: this.state.currPage + 1 });
    }

    onLast(e) {
        this.setState({ currPage: this.state.lastPage });
    }

    onFirst(e) {
        this.setState({ currPage: 1 });
    }

    UserList() {
        let currPage = this.state.currPage;
        let usersAmount = this.state.usersAmount;
        let admins = this.state.admins;
        let i = (currPage - 1) * usersAmount;

        let adminsList = [];
        for (; i < (currPage - 1) * usersAmount + usersAmount && i < admins.length; i++) {
            adminsList.push(<User user={admins[i]} key={admins[i]._id} />);
        }
        return adminsList;
    }

    render() {
        const firstPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const lastPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const elsePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const onePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )

        let pagination;

        if (this.state.lastPage === 1)
            pagination = onePagePagination;
        else if (this.state.currPage === 1)
            pagination = firstPagePagination;
        else if (this.state.currPage === this.state.lastPage)
            pagination = lastPagePagination;
        else
            pagination = elsePagePagination

        const addAdminUrl = '/companies/' + this.props.match.params.id + '/admins/add';

        return (
            <div className="container">
                <h3>Admins</h3>
                {this.state.isAdmin ? <Link to={addAdminUrl}>
                    <button type="button" className="btn btn-dark">Add/Remove an Admin</button>
                </Link> : ''}

                {this.state.lastPage ?
                    <>
                        <table className="table">
                            <thead className="thead-light">
                                <tr>
                                    <th></th>
                                    <th>Login</th>
                                    <th>Email</th>
                                    <th>Fullname</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.UserList()}
                            </tbody>
                        </table>
                        <nav aria-label="pagination">
                            {pagination}
                        </nav>
                    </>
                    : 'Admins not found'}

            </div>
        )
    }
}