import React, { Component } from 'react';

export default class MessageSent extends Component {

    componentDidMount() {
        document.title = "Sent";
    }

    render() {
        return (
            <div className="container">
                <h2>Done</h2>
                <p>We have sent a message on your E-mail. Please, follow the instructions in the message</p>
            </div>
        )
    }
}