import React, { Component } from 'react';

export default class Unauthorized extends Component {
    
    componentDidMount() {
        document.title = "Unauthorized";
    }

    render() {
        return (
            <div className="container">
              <h1>401: Unauthorized</h1>
          </div>
        )
    }
}