import React, { Component } from 'react';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

export default class PasswordReset extends Component {
    constructor(props) {
        super(props);

        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            email: '',
            message: '',
            sendBtnDisabled: false
        }
    }

    onChangeEmail(e) {
        this.setState({ message: '' });
        this.setState({ email: e.target.value })
    }

    componentDidMount() {
        if (this.props.location.search.length > 0)
            this.setState({ message: "Bad Token" });

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            this.setState({ isLoggedIn: true });
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };

            axios.get(url + '/users/me', reqOptions)
                .then(user => {
                    if (user.data) {
                        this.props.history.push("/");
                    }
                })
        }
    }

    onSubmit(e) {
        e.preventDefault();

        this.setState({ sendBtnDisabled: true });

        const user = {
            email: this.state.email
        }

        axios.post(url + '/auth/passwordreset', user)
            .then(resetResult => {
                this.props.history.push("/message/sent");
            })
            .catch(err => {
                console.log(err.message);
                this.setState({ message: "Wrong E-mail" });
                this.setState({ sendBtnDisabled: false });
            })
    }

    render() {

        let sendBtn = (
            <div className="form-group">
                <input type="submit"
                    value="Send"
                    className="btn btn-dark"
                />
            </div>
        )
        if (this.state.sendBtnDisabled) {
            sendBtn = (
                <div className="form-group">
                    <input type="submit"
                        value="Send"
                        className="btn btn-dark"
                        disabled
                    />
                </div>
            )
        }

        return (
            <div className="container">
                <div className="form-group">
                    <h3>Password Reset</h3>
                </div>
                <div className="form-group">
                    Forgotten your password? Enter your e-mail address below, and we'll send you an e-mail allowing you to reset it
                </div>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <input type="email"
                            required
                            className="form-control"
                            value={this.state.email}
                            onChange={this.onChangeEmail}
                        />
                    </div>
                    <div className="form-group">
                        <p>{this.state.message}</p>
                    </div>
                    {sendBtn}
                </form>
            </div>
        )
    }
}