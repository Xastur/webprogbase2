import React, { Component } from 'react';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

export default class CompanyCreate extends Component {

    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeCompanyName = this.onChangeCompanyName.bind(this);
        this.onChangeAboutCompany = this.onChangeAboutCompany.bind(this);
        this.onChangeSelectedFile = this.onChangeSelectedFile.bind(this);

        this.state = {
            _id: '',
            companyName: '',
            aboutCompany: '',
            selectedFile: '',
            message: '',
            saveBtnDisabled: false
        }
    }

    componentDidMount() {
        document.title = "New Company";

        const jwt = localStorage.getItem('jwt');
        if (!jwt) {
            this.props.history.push('/unauthorized');
        }
    }

    onSubmit(e) {
        e.preventDefault();

        const jwt = localStorage.getItem('jwt');

        if (jwt) {
            if (this.state.selectedFile) {
                this.setState({ saveBtnDisabled: true });

                const fd = new FormData();

                fd.append('image', this.state.selectedFile, this.state.selectedFile.name);
                fd.append('companyName', this.state.companyName)
                fd.append('aboutCompany', this.state.aboutCompany)

                const reqOptions = {
                    headers: {
                        Authorization: `Bearer ${jwt}`,
                    }
                };

                axios.post(url + '/companies/create', fd, reqOptions)
                    .then(company => {
                        this.props.history.push('/companies/' + company.data._id);
                    })
                    .catch(err => {
                        this.setState({ saveBtnDisabled: false });
                        this.setState({ message: "Error" });
                    })
            }
            else {
                this.setState({ message: "Photo is required" });
                this.setState({ saveBtnDisabled: false });
            }
        }
        else
            this.props.history.push("/unauthorized");
    }

    onChangeCompanyName(e) {
        this.setState({ message: '' });
        this.setState({ companyName: e.target.value });
    }

    onChangeAboutCompany(e) {
        this.setState({ message: '' });
        this.setState({ aboutCompany: e.target.value });
    }

    onChangeSelectedFile(e) {
        this.setState({ message: '' });
        this.setState({ selectedFile: e.target.files[0] });
    }

    render() {

        let saveBtn = (
            <div className="form-group">
                <input type="submit"
                    value="Save"
                    className="btn btn-success"
                />
            </div>
        )
        if (this.state.saveBtnDisabled) {
            saveBtn = (
                <div className="form-group">
                    <input type="submit"
                        value="Save"
                        className="btn btn-success"
                        disabled
                    />
                </div>
            )
        }

        return (
            <div className="container">
                <h3>Create Company</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label id="textStyle">Company Name: *</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.companyName}
                            onChange={this.onChangeCompanyName}
                            id="textStyle"
                        />
                        <br />
                        <label id="textStyle">About Company: </label>
                        <textarea className="form-control"
                            value={this.state.aboutCompany}
                            onChange={this.onChangeAboutCompany}
                            rows="8" cols="50"
                            id="textStyle"
                        />
                        <p>{this.state.message}</p>
                    </div>
                    <label id="textStyle">Choose image: *</label><br />
                    <input className="btn btn-dark" type="file" name="selectedFile" onChange={this.onChangeSelectedFile} /><br /><br />
                    {saveBtn}
                </form>
            </div>
        )
    }
}