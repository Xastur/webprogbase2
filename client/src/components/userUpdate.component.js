import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';
import config from '../config/config';

const url = config.url;

function refreshPage() {
    window.location.reload(false);
}

export default class UserUpdate extends Component {

    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onChangeFullname = this.onChangeFullname.bind(this);
        this.onChangeBiography = this.onChangeBiography.bind(this);
        this.onChangeSelectedFile = this.onChangeSelectedFile.bind(this);
        this.onChangeOpenModal = this.onChangeOpenModal.bind(this);

        this.state = {
            _id: '',
            currUserId: '',
            role: '',
            fullname: '',
            biography: '',
            selectedFile: '',
            openModal: false,
            owner: false,
            loading: true,
            message: '',
            saveBtnDisabled: false,
            deleteBtnDisabled: false
        }
    }

    componentDidMount() {
        document.title = "Update User";

        const jwt = localStorage.getItem('jwt');
        if (!jwt) {
            console.log("JWT is missing!");
            this.props.history.push("/");
        }
        else {
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };

            axios.get(url + '/users/me', reqOptions)
                .then(user => {
                    this.setState({ currUserId: user.data.user._id });
                    this.setState({ _id: user.data.user._id });
                    this.setState({ role: user.data.user.role });

                    return axios.get(url + '/users/' + this.props.match.params.id, reqOptions);
                })
                .then(user => {
                    if (user.data) {
                        if (this.state._id !== user.data._id) {
                            if (this.state.role) {

                                this.setState({ fullname: user.data.fullname });
                                this.setState({ biography: user.data.biography });
                                this.setState({ _id: user.data._id });
                                this.setState({ owner: true });
                            }
                            else {
                                this.props.history.push("/forbidden");
                                return;
                            }
                        }
                        else {
                            this.setState({ owner: true });
                            this.setState({ fullname: user.data.fullname });
                            this.setState({ biography: user.data.biography });
                        }
                        this.setState({ loading: false });
                    }
                    else
                        this.props.history.push("/notfound");
                })
                .catch(err => {
                    this.props.history.push("/");
                })
        }
    }

    onSubmit(e) {
        e.preventDefault();

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const fd = new FormData();

            this.setState({ saveBtnDisabled: true });
            this.setState({ deleteBtnDisabled: true });

            if (this.state.selectedFile)
                fd.append('image', this.state.selectedFile, this.state.selectedFile.name)

            fd.append('biography', this.state.biography)
            fd.append('fullname', this.state.fullname)

            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            axios.put(url + '/users/' + this.state._id + '/update', fd, reqOptions)
                .then(x => {
                    this.props.history.push('/users/' + this.state._id);
                })
                .catch(err => {
                    console.log(err.message);
                    this.setState({ message: "Error" })
                    this.setState({ saveBtnDisabled: false });
                    this.setState({ deleteBtnDisabled: false });
                })
        }
        else
            this.props.history.push("/unauthorized");
    }

    onChangeFullname(e) {
        this.setState({ message: '' });
        this.setState({ fullname: e.target.value })
    }

    onChangeBiography(e) {
        this.setState({ message: '' });
        this.setState({ biography: e.target.value })
    }

    onChangeSelectedFile(e) {
        this.setState({ message: '' });
        this.setState({ selectedFile: e.target.files[0] })
    }

    onDelete(e) {

        this.setState({ openModal: false })
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            axios.delete(url + '/users/' + this.state._id + '/delete', reqOptions)
                .then(x => {

                    if (this.state._id === this.state.currUserId) {
                        localStorage.removeItem('jwt');
                        refreshPage();
                        this.props.history.push("/");
                    }
                    else
                        this.props.history.push("/users");
                })
                .catch(err => {
                    console.log(err.message);
                    this.setState({ message: "Error" });
                    this.setState({ saveBtnDisabled: false });
                    this.setState({ deleteBtnDisabled: false });
                });
        }
        else
            this.props.history.push("/");
    }

    onChangeOpenModal(e) {
        if (this.state.openModal)
            this.setState({ openModal: false })
        else
            this.setState({ openModal: true })
    }

    render() {
        let deleteBtn = (
            <input type="submit"
                className="btn btn-danger"
                value="Delete account"
                onClick={this.onChangeOpenModal}
                data-toggle="modal"
                data-target="#basicExampleModal"
            />
        )
        if (this.state.deleteBtnDisabled) {
            deleteBtn = (
                <input type="submit"
                    className="btn btn-danger"
                    value="Delete account"
                    onClick={this.onChangeOpenModal}
                    data-toggle="modal"
                    data-target="#basicExampleModal"
                    disabled
                />
            )
        }
        let saveBtn = (
            <div className="form-group">
                <input type="submit"
                    value="Save"
                    className="btn btn-success" />
            </div>
        )
        if (this.state.saveBtnDisabled) {
            saveBtn = (
                <div className="form-group">
                    <input type="submit"
                        value="Save"
                        className="btn btn-success"
                        disabled
                    />
                </div>
            )
        }

        return (
            <div className="container">
                {this.state.loading ? "Loading..."
                    :
                    <>
                        <h3>Update User</h3>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label id="textStyle">Fullname: *</label>
                                <input type="text"
                                    required
                                    className="form-control"
                                    value={this.state.fullname}
                                    onChange={this.onChangeFullname}
                                    id="textStyle"
                                />
                                <br />
                                <label id="textStyle">Biography: </label>
                                <textarea className="form-control"
                                    value={this.state.biography}
                                    onChange={this.onChangeBiography}
                                    rows="8" cols="50"
                                    id="textStyle"
                                />
                                <p id="textStyle">{this.state.message}</p>
                            </div>
                            Change image: <input className="btn btn-dark" type="file" name="selectedFile" onChange={this.onChangeSelectedFile} /><br />
                            {saveBtn}
                        </form>

                        {this.state.owner ? deleteBtn : ''}

                        <Modal isOpen={this.state.openModal} className={"className"}>
                            <ModalHeader>On Delete</ModalHeader>
                            <ModalBody>
                                Are you sure?
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" onClick={this.onDelete}>Confirm</Button>{' '}
                                <Button color="secondary" onClick={this.onChangeOpenModal}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                    </>
                }
            </div>
        )
    }
}