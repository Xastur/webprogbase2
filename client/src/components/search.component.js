import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

const minAmount = 2;

const Blog = props => (
    <tr>
        <td><Link to={"/blogs/" + props.blog._id} className="text-dark"><img height="128" src={props.blog.imagesSrc[0]} alt="img" /></Link></td>
        <td><Link to={"/blogs/" + props.blog._id} className="text-dark">{props.blog.artTitle}</Link></td>
        <td>{props.blog.likesId.length}</td>
    </tr>
)

const User = props => (
    <tr>
        <td><Link to={"/users/" + props.user._id} className="text-dark"><img height="64" src={props.user.avaUrl} alt="img" /></Link></td>
        <td><Link to={"/users/" + props.user._id} className="text-dark">{props.user.login}</Link></td>
        <td>{props.user.fullname}</td>
    </tr>
)

const Company = props => (
    <tr>
        <td><Link to={"/companies/" + props.company._id} className="text-dark"><img height="128" src={props.company.imageSrc} alt="img" /></Link></td>
        <td><Link to={"/companies/" + props.company._id} className="text-dark">{props.company.companyName}</Link></td>
    </tr>
)

export default class Search extends Component {

    constructor(props) {
        super(props);

        this.onShowMoreUsersClick = this.onShowMoreUsersClick.bind(this);
        this.onShowLessUsersClick = this.onShowLessUsersClick.bind(this);
        this.onShowMoreBlogsClick = this.onShowMoreBlogsClick.bind(this);
        this.onShowLessBlogsClick = this.onShowLessBlogsClick.bind(this);
        this.onShowMoreCompaniesClick = this.onShowMoreCompaniesClick.bind(this);
        this.onShowLessCompaniesClick = this.onShowLessCompaniesClick.bind(this);

        this.state = {
            _id: '',
            path: '',
            users: [],
            blogs: [],
            companies: [],
            usersListAmount: minAmount,
            blogsListAmount: minAmount,
            companiesListAmount: minAmount,
            loading: true,
            showMoreUsersDisabled: false,
            showMoreBlogsDisabled: false,
            showMoreCompaniesDisabled: false
        }
    }

    onShowMoreUsersClick(e) {
        if (this.state.users.length <= this.state.usersListAmount * 2)
            this.setState({ showMoreUsersDisabled: true })

        if (this.state.users.length > this.state.usersListAmount) {
            this.setState({ usersListAmount: this.state.usersListAmount * 2 });
        }
    }

    onShowLessUsersClick(e) {
        if (this.state.usersListAmount / 2 > minAmount)
            this.setState({ showMoreUsersDisabled: false })

        if (this.state.usersListAmount >= minAmount)
            this.setState({ usersListAmount: this.state.usersListAmount / 2 })
    }

    onShowMoreBlogsClick(e) {
        if (this.state.blogs.length <= this.state.blogsListAmount * 2)
            this.setState({ showMoreBlogsDisabled: true });

        if (this.state.blogs.length > this.state.blogsListAmount)
            this.setState({ blogsListAmount: this.state.blogsListAmount * 2 });
    }

    onShowLessBlogsClick(e) {
        if (this.state.blogsListAmount / 2 >= minAmount)
            this.setState({ showMoreBlogsDisabled: false });

        if (this.state.blogsListAmount > minAmount)
            this.setState({ blogsListAmount: this.state.blogsListAmount / 2 })
    }

    onShowMoreCompaniesClick(e) {
        if (this.state.companies.length <= this.state.companiesListAmount * 2)
            this.setState({ showMoreCompaniesDisabled: true });

        if (this.state.companies.length > this.state.companiesListAmount)
            this.setState({ companiesListAmount: this.state.companiesListAmount * 2 });
    }

    onShowLessCompaniesClick(e) {
        if (this.state.companiesListAmount / 2 >= minAmount)
            this.setState({ showMoreCompaniesDisabled: false });

        if (this.state.companiesListAmount > minAmount)
            this.setState({ companiesListAmount: this.state.companiesListAmount / 2 })
    }

    UserList() {
        let i = 0;
        return this.state.users.map(currentUser => {
            if (i < this.state.usersListAmount && !currentUser.isDisabled) {
                i++;
                return <User user={currentUser} key={currentUser._id} />;
            }
            return null;
        })
    }

    BlogList() {
        let i = 0;
        return this.state.blogs.map(currentBlog => {
            if (i < this.state.blogsListAmount) {
                i++;
                return <Blog blog={currentBlog} key={currentBlog._id} />;
            }
            return null;
        })
    }

    CompanyList() {
        let i = 0;
        return this.state.companies.map(currentCompany => {
            if (i < this.state.companiesListAmount) {
                i++;
                return <Company company={currentCompany} key={currentCompany._id} />;
            }
            return null;
        })
    }

    componentDidMount() {
        document.title = "Search";

        this.setState({ path: window.location.pathname });
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            this.setState({ isLoggedIn: true });
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };
            axios.get(url + window.location.pathname, reqOptions)
                .then(resArray => {
                    resArray.data.forEach(element => {
                        if (element.login)
                            this.setState({ users: this.state.users.concat(element) })
                        else if (element.artTitle)
                            this.setState({ blogs: this.state.blogs.concat(element) })
                        else
                            this.setState({ companies: this.state.companies.concat(element) })
                    });
                    console.log("USERS:", this.state.users);
                    console.log("BLOGS:", this.state.blogs);
                    console.log("COMPANIES:", this.state.companies);

                    this.setState({ loading: false });
                })
                .catch(err => {
                    console.log(err.message);
                    this.props.history.push('/');
                })
        }
        else
            this.props.history.push('/unauthorized');
    }

    // static getDerivedStateFromProps(nextProps, prevState) {
    //     if (nextProps.location.pathname && prevState.path) {

    //         // prevState.path.replace("%", "0");
    //         // console.log("11:", nextProps.location.pathname, "\n22:", prevState.path);
    //         // console.log(prevState.path.indexOf("%20"));
    //         // prevState.path.splice(prevState.path.indexOf("%20"), 3);

    //         // var str = "Mr Blue has a blue house and a blue car";
    //         // var res = str.replace(/blue/g, "red");
    //         let path = prevState.path.replace(/%20/g, " ");
    //         let path2 = prevState.path;
    //         console.log("33:", nextProps.location.pathname, "\n44:", path, "\n55:", path2);

    //         // likesId.splice(likesId.indexOf(ObjectId(req.body.userId)), 1);

    //         // let path = [];

    //         // for (let i = 0; i < prevState.path.length; i++) {
    //         //     if (prevState.path[i] !== "%")
    //         //         path += prevState.path[i];
    //         //     else
    //         //         path += " ";
    //         //     // console.log(prevState.path[i]);
    //         // }

    //         if (nextProps.location.pathname !== path) {
    //             // window.location.reload(false);
    //         }
    //     }
    //     return null;
    // }

    render() {
        let usersList = null;
        let blogsList = null;
        let companiesList = null;
        let notFound;

        if (this.state.users.length > 0) {
            usersList = (
                <>
                    <h3>Users</h3>
                    <table className="table">
                        <thead className="thead-light">
                            <tr>
                                <th></th>
                                <th>Login</th>
                                <th>Fullname</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.UserList()}
                        </tbody>
                    </table>

                    {(this.state.showMoreUsersDisabled || this.state.users.length <= minAmount) ? "" :
                        <button type="button" className="btn btn-light" onClick={this.onShowMoreUsersClick}>show more</button>
                    }
                    {this.state.usersListAmount === minAmount ? "" :
                        <button type="button" className="btn btn-light" onClick={this.onShowLessUsersClick}>show less</button>
                    }
                    <br />
                </>
            )
        }

        if (this.state.blogs.length > 0) {
            blogsList = (
                <>
                    <h3>Blogs</h3>
                    <table className="table">

                        <thead className="thead-light">
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th>Likes</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.BlogList()}
                        </tbody>
                    </table>
                    {(this.state.showMoreBlogsDisabled || this.state.blogs.length <= minAmount) ? "" :
                        <button type="button" className="btn btn-light" onClick={this.onShowMoreBlogsClick}>show more</button>
                    }
                    {this.state.blogsListAmount === minAmount ? "" :
                        <button type="button" className="btn btn-light" onClick={this.onShowLessBlogsClick}>show less</button>
                    }
                    <br />
                </>
            )
        }

        if (this.state.companies.length > 0) {
            companiesList = (
                <>
                    <h3>Companies</h3>
                    <table className="table">

                        <thead className="thead-light">
                            <tr>
                                <th></th>
                                <th>Company Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.CompanyList()}
                        </tbody>
                    </table>

                    {(this.state.showMoreCompaniesDisabled || this.state.companies.length <= minAmount) ? "" :
                        <button type="button" className="btn btn-light" onClick={this.onShowMoreCompaniesClick}>show more</button>
                    }
                    {this.state.companiesListAmount === minAmount ? "" :
                        <button type="button" className="btn btn-light" onClick={this.onShowLessCompaniesClick}>show less</button>
                    }
                    <br />
                </>
            )
        }

        if (!usersList && !blogsList && !companiesList)
            notFound = (
                <>
                    <h3>Not Found :(</h3>
                </>
            )

        return (
            <div className="container">
                {this.state.loading ? "Searching.."
                    :
                    <>
                        <h2>Search</h2>
                        <br />
                        {usersList}
                        <br />
                        {blogsList}
                        <br />
                        {companiesList}
                        {notFound}
                    </>
                }
            </div>
        )
    }
}