import React, { Component } from 'react';
import axios from 'axios';
import config from '../config/config';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
const url = config.url;

export default class BlogUpdate extends Component {

    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onChangePassportData = this.onChangePassportData.bind(this);
        this.onChangeAboutMyCar = this.onChangeAboutMyCar.bind(this);
        this.onChangeSelectedFiles = this.onChangeSelectedFiles.bind(this);
        this.onChangeOpenModal = this.onChangeOpenModal.bind(this);

        this.state = {
            _id: '',
            artTitle: '',
            passportData: '',
            aboutMyCar: '',
            selectedFiles: [],
            commentsId: [],
            openModal: false,
            loading: true,
            message: '',
            saveBtnDisabled: false,
            deleteBtnDisabled: false
        }
    }

    componentDidMount() {
        document.title = "Update Blog";

        const jwt = localStorage.getItem('jwt');
        if (!jwt) 
            this.props.history.push("/unauthorized");
        else {
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}` },
            };
            axios.get(url + '/users/me', reqOptions)
                .then(user => {
                    if (user.data.user.role)
                        return axios.get(url + '/blogs/' + this.props.match.params.id, reqOptions)
                    else {
                        for (let i = 0; i < user.data.user.articlesId.length; i++) {
                            if (user.data.user.articlesId[i] === this.props.match.params.id) {
                                return axios.get(url + '/blogs/' + this.props.match.params.id, reqOptions)
                            }
                        }
                        // console.log("Forbidden");
                        this.props.history.push('/forbidden');
                        return;
                    }
                })
                .then(blog => {
                    if (blog) {
                        this.setState({ _id: blog.data._id });
                        this.setState({ artTitle: blog.data.artTitle });
                        this.setState({ passportData: blog.data.passportData });
                        this.setState({ aboutMyCar: blog.data.aboutMyCar });
                        this.setState({ commentsId: blog.data.commentsId });
                        this.setState({ loading: false });
                    }
                })
                .catch(err => {
                    this.props.history.push('/notfound');
                })
        }
    }

    onSubmit(e) {
        e.preventDefault();

        const jwt = localStorage.getItem('jwt');

        if (jwt) {
            const fd = new FormData();

            this.setState({ message: "Please, wait" });
            this.setState({ saveBtnDisabled: true });
            this.setState({ deleteBtnDisabled: true });

            if (this.state.selectedFiles.length > 0) {
                for (let i = 0; i < this.state.selectedFiles.length && i < 10; i++) {
                    fd.append('image', this.state.selectedFiles[i]);
                }
            }

            fd.append('artTitle', this.state.artTitle);
            fd.append('aboutMyCar', this.state.aboutMyCar);
            fd.append('passportData', this.state.passportData);

            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            axios.put(url + '/blogs/' + this.state._id + '/update', fd, reqOptions)
                .then(x => {
                    this.props.history.push('/blogs/' + this.state._id);
                })
                .catch(err => {
                    console.log(err.message);
                    this.state({ message: "Error" });
                    this.setState({ saveBtnDisabled: false });
                    this.setState({ deleteBtnDisabled: false });
                })
        }
        else {
            this.props.history.push("/unauthorized");
        }
    }

    onChangeTitle(e) {
        this.setState({ message: '' });
        this.setState({
            artTitle: e.target.value
        })
    }

    onChangePassportData(e) {
        this.setState({ message: '' });
        this.setState({ passportData: e.target.value })
    }

    onChangeAboutMyCar(e) {
        this.setState({ message: '' });
        this.setState({ aboutMyCar: e.target.value })
    }

    onChangeSelectedFiles(e) {
        this.setState({ message: '' });
        this.setState({ selectedFiles: e.target.files })
    }

    onDelete(e) {
        this.setState({ openModal: false })
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            let fd = new FormData();
            let subscribersId;

            axios.get(url + '/blogs/' + this.state._id, reqOptions)
                .then(blog => {
                    if (blog.data) {
                        return axios.get(url + '/users/' + blog.data.userId, reqOptions);
                    }
                })
                .then(user => {
                    if (user.data) {
                        subscribersId = user.data.subscribersId;
                        return axios.delete(url + '/blogs/' + this.state._id + '/delete', reqOptions);
                    }
                })
                .then(x => {
                    if (x.data) {
                        fd.append('subscribersId', subscribersId);
                        fd.append('blogId', this.state._id);
                        return axios.put(url + '/users/update/subBlogsId/delete', fd, reqOptions);
                    }
                })
                .then(x => {
                    this.props.history.push("/blogs");
                })
                .catch(err => {
                    console.log(err.message);
                    this.setState({ message: "Error" });
                    this.setState({ saveBtnDisabled: false });
                    this.setState({ deleteBtnDisabled: false });
                });
        }
        else
            this.props.history.push("/");
    }

    onChangeOpenModal(e) {
        if (this.state.openModal)
            this.setState({ openModal: false })
        else
            this.setState({ openModal: true })
    }

    render() {

        let deleteBtn = (
            <input type="submit"
                className="btn btn-danger"
                value="Delete blog"
                onClick={this.onChangeOpenModal}
                data-toggle="modal"
                data-target="#basicExampleModal"
            />
        )
        if (this.state.deleteBtnDisabled) {
            deleteBtn = (
                <input type="submit"
                    className="btn btn-danger"
                    value="Delete blog"
                    onClick={this.onChangeOpenModal}
                    data-toggle="modal"
                    data-target="#basicExampleModal"
                    disabled
                />
            )
        }
        let saveBtn = (
            <div className="form-group">
                <input type="submit"
                    value="Save"
                    className="btn btn-success"
                />
            </div>
        )
        if (this.state.saveBtnDisabled) {
            saveBtn = (
                <div className="form-group">
                    <input type="submit"
                        value="Save"
                        className="btn btn-success"
                        disabled
                    />
                </div>
            )
        }

        return (
            <div className="container">
                <h3>Update Blog</h3>
                {this.state.loading ? "Loading..."
                    :
                    <>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label id="textStyle">Title *</label>
                                <input type="text"
                                    required
                                    className="form-control"
                                    value={this.state.artTitle}
                                    onChange={this.onChangeTitle}
                                    id="textStyle"
                                />
                                <br />
                                <label id="textStyle">About My Car: </label>
                                <textarea className="form-control"
                                    value={this.state.aboutMyCar}
                                    onChange={this.onChangeAboutMyCar}
                                    rows="8" cols="50"
                                    id="textStyle"
                                />
                                <br />
                                <label id="textStyle">Passport data: </label>
                                <textarea className="form-control"
                                    value={this.state.passportData}
                                    onChange={this.onChangePassportData}
                                    rows="5" cols="50"
                                    id="textStyle"
                                />
                                <p id="textStyle">{this.state.message}</p>
                            </div>
                            <p id="textStyle">Change image: </p>
                            <input className="btn btn-dark" type="file" name="selectedFiles" onChange={this.onChangeSelectedFiles} multiple />
                            <br />
                            <br />

                            {saveBtn}
                        </form>

                        {deleteBtn}

                        <Modal isOpen={this.state.openModal} className={"className"}>
                            <ModalHeader>On Delete</ModalHeader>
                            <ModalBody>
                                Are you sure?
                    </ModalBody>
                            <ModalFooter>
                                <Button color="primary" onClick={this.onDelete}>Confirm</Button>{' '}
                                <Button color="secondary" onClick={this.onChangeOpenModal}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                    </>
                }

            </div>
        )
    }
}