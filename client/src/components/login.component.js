import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

function refreshPage() {
    window.location.reload(false);
}

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            email: '',
            password: '',
            message: '',
            submitBtnDisabled: false,
            forgotBtnDisabled: false
        }
    }

    onChangeEmail(e) {
        this.setState({ message: '' });
        this.setState({ email: e.target.value });
    }

    onChangePassword(e) {
        this.setState({ message: '' });
        this.setState({ password: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();

        this.setState({ message: "Please, wait" });
        this.setState({ submitBtnDisabled: true });
        this.setState({ forgotBtnDisabled: true });

        const user = {
            email: this.state.email,
            password: this.state.password
        }

        axios.post(url + '/auth/login', user)
            .then(authResult => {
                const jwt = authResult.data.token;
                localStorage.setItem("jwt", jwt);
                this.props.history.push("/");
                refreshPage();
            })
            .catch(err => {
                console.log(err.message);
                this.setState({ message: "Wrong E-mail or password" });
                this.setState({ submitBtnDisabled: false });
                this.setState({ forgotBtnDisabled: false });
            })

        this.setState({
            username: '',
            password: ''
        })
    }

    componentDidMount() {
        document.title = "Login";
    }

    render() {

        const forgotPasswordUrl = '/auth/passwordreset';

        let submitBtn = (
            <div className="form-group">
                <input type="submit"
                    value="Log In"
                    className="btn btn-dark"
                />
            </div>
        )
        if (this.state.submitBtnDisabled) {
            submitBtn = (
                <div className="form-group">
                    <input type="submit"
                        value="Log In"
                        className="btn btn-dark"
                        disabled
                    />
                </div>
            )
        }
        let forgotBtn = (
            <input type="submit"
                value="Forgot Password"
                className="btn btn-dark"
            />
        )
        if (this.state.forgotBtnDisabled) {
            forgotBtn = (
                <input type="submit"
                    value="Forgot Password"
                    className="btn btn-dark"
                    disabled
                />
            )
        }

        return (
            <div className="container">
                <div className="form-group">
                    <h3>Log In</h3>
                </div>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>E-mail: </label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.email}
                            onChange={this.onChangeEmail}
                        />
                    </div>
                    <div className="form-group">
                        <label>Password: </label>
                        <input type="password"
                            required
                            className="form-control"
                            value={this.state.password}
                            onChange={this.onChangePassword}
                        />
                        <p>{this.state.message}</p>
                    </div>
                    {submitBtn}
                    <div className="form-group">
                        <Link to={forgotPasswordUrl}>
                            {forgotBtn}
                        </Link>
                    </div>
                </form>
            </div>
        )
    }
}