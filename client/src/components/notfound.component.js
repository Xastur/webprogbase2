import React, { Component } from 'react';

export default class NotFound extends Component {
    
    componentDidMount() {
        document.title = "Not Found";
    }

    render() {
        return (
            <div className="container">
              <h1>404: Not Found</h1>
          </div>
        )
    }
}