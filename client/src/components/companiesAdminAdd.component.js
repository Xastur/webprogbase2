import React, { Component } from 'react';
import axios from 'axios';
import config from '../config/config';

const url = config.url;

export default class CompanyAdminAdd extends Component {

    constructor(props) {
        super(props);

        this.onChangeId = this.onChangeId.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            companyId: '',
            currUserId: '',
            typedId: '',
            message: '',
            foundLogin: '',
            foundFullname: '',
            foundAvaUrl: '',
            foundEmail: '',
            buttonText: 'Add',
            userFound: false
        }

    }

    onChangeId(e) {
        this.setState({
            typedId: e.target.value
        })
        this.setState({ message: '' });
        this.setState({ userFound: false });
        this.setState({ buttonText: 'Add' });

        if (e.target.value.length === 24) {

            const jwt = localStorage.getItem('jwt');
            if (jwt) {
                const reqOptions = {
                    headers: {
                        Authorization: `Bearer ${jwt}`,
                    }
                }
                axios.get(url + '/users/' + e.target.value, reqOptions)
                    .then(user => {
                        if (user.data) {
                            console.log(user.data);
                            for (let i = 0; i < user.data.companiesId.length; i++) {
                                if (user.data.companiesId[i] === this.state.companyId) {
                                    this.setState({ message: "This user is currently an admin. Remove the admin?" });
                                    this.setState({ buttonText: 'Remove' });
                                }
                            }
                            this.setState({ foundLogin: user.data.login });
                            this.setState({ foundFullname: user.data.fullname });
                            this.setState({ foundAvaUrl: user.data.avaUrl });
                            this.setState({ foundEmail: user.data.email });
                            this.setState({ userFound: true });
                        }
                        else {
                            this.setState({ userFound: false });
                            this.setState({ message: 'Wrong user Id' });
                        }
                    })
                    .catch(err => this.setState({ message: 'Wrong user Id' }));
            }
            else
                this.props.history.push("/");
        }
        else
            this.setState({ userFound: false });
    }


    componentDidMount() {
        document.title = "Add Admin";

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            }

            this.setState({ companyId: this.props.match.params.id });

            axios.get(url + '/users/me', reqOptions)
                .then(user => {
                    this.setState({ currUserId: user.data.user._id });

                    return axios.get(url + '/companies/' + this.state.companyId, reqOptions);
                })
                .then(company => {

                    for (let i = 0; i < company.data.admins.length; i++) {
                        if (company.data.admins[i] === this.state.currUserId) {
                            return;
                        }
                    }
                    this.props.history.push("/");
                    return;
                })
                .catch(err => {
                    this.setState({ message: err.message });
                });
        }
        else
            this.props.history.push("/unauthorized");
    }

    onSubmit(e) {
        e.preventDefault();

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            }

            if (this.state.typedId.length !== 24)
                this.setState({ message: "Id must have 24 chars long" });
            else {
                if (this.state.userFound) {

                    const fd = new FormData();
                    fd.append('companyId', this.state.companyId);

                    axios.put(url + '/users/' + this.state.typedId + '/update', fd, reqOptions)
                        .then(x => {
                            fd.append('admin', this.state.typedId);

                            return axios.put(url + '/companies/' + this.state.companyId + '/update', fd, reqOptions)
                        })
                        .then(x => {
                            this.props.history.push("/companies/" + this.state.companyId + '/admins');
                        })
                        .catch(err => console.log(err))
                }
            }
        }
        else
            this.props.history.push("/unauthorized");
    }

    render() {

        const foundAva = (
            <img src={this.state.foundAvaUrl} alt="img" width="128" height="128" />
        )

        return (
            <div className="container">
                <h3>Add/Remove an Admin</h3>

                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Type user id you want to add/remove:</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.typedId}
                            onChange={this.onChangeId}
                        />
                    </div>

                    {this.state.userFound ?
                        <>
                            {foundAva} < br />
                            {"Login: " + this.state.foundLogin} < br />
                            {"Fullname: " + this.state.foundFullname} < br />
                            {"E-mail: " + this.state.foundEmail} < br />
                        </>
                        :
                        ''
                    }
                    <br />
                    {this.state.message}
                    <div className="form-group">
                        <input type="submit" value={this.state.buttonText} className="btn btn-dark" />
                    </div>
                </form>
            </div>
        )
    }
}
