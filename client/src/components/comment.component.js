import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

function refreshPage() {
    window.location.reload(false);
}

export default class Comment extends Component {

    constructor(props) {
        super(props);

        this.onDelete = this.onDelete.bind(this);

        this.state = {
            comment: '',
            _id: this.props.id,
            userLogin: '',
            userId: '',
            userAvaUrl: '',
            createdAt: '',
            owner: false,
            loading: true,
            blogId: this.props.blogId,
            date: ''
        }
    }

    componentDidMount() {
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            axios.get(url + '/comments/' + this.state._id, reqOptions)
                .then(comment => {
                    this.setState({ comment: comment.data.comment });
                    // this.setState({ userLogin: comment.data.userLogin });
                    this.setState({ userId: comment.data.userId });
                    this.setState({ createdAt: comment.data.createdAt });
                    // this.setState({ userAvaUrl: comment.data.userAvaUrl });
                    this.setState({ date: comment.data.createdAt });

                    console.log(comment.data.createdAt);

                    return axios.get(url + '/users/me', reqOptions)
                })
                .then(user => {
                    if (user.data.user._id === this.state.userId || user.data.user.role)
                        this.setState({ owner: true });

                    this.setState({ loading: false });

                    return axios.get(url + '/users/' + this.state.userId, reqOptions);
                })
                .then(user => {
                    this.setState({ userLogin: user.data.login });
                    this.setState({ userAvaUrl: user.data.avaUrl });
                })
                .catch(err => console.log(err))
        }
        else
            this.props.history.push("/unauthorized");
    }

    onDelete(e) {
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            let fd = new FormData();
            fd.append('commentId', this.state._id);

            axios.delete(url + '/comments/' + this.state._id + '/delete', reqOptions)
                .then(x => {
                    return axios.put(url + '/blogs/' + this.state.blogId + '/deletecomment', fd, reqOptions)
                })
                .then(x => refreshPage())
                .catch(err => console.log(err))
        }
        else
            this.props.history.push("/");
    }

    render() {

        const deleteText = (
            <div className="mt-0 mb-1 text-muted" id="deleteTextId" onClick={this.onDelete}>(delete comment)</div>
        )

        const body = (
            <>
                <img
                    className="mr-3 bg-light rounded"
                    width="48"
                    height="48"
                    src={this.state.userAvaUrl}
                    alt='ava'
                />
                {/* https://api.adorable.io/avatars/48/name@adorable.io.png */}
                <div className="media-body p-2 shadow-sm rounded bg-light border">
                    {/* <small className="float-right text-muted">{this.state.date}</small> */}

                    {/* <p type="datetime" name="created"></p> */}
                    <h6 id="loginComment"><Link to={"/users/" + this.state.userId} className="mt-0 mb-1 text-muted">{this.state.userLogin}</Link></h6>
                    {this.state.owner ? deleteText : ''}
                    <br />
                    {this.state.comment}
                </div>
            </>
        )

        return (
            <div className="media mb-3">
                {this.state.loading ? '' : body}
            </div>
        )
    }
}