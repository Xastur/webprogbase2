import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

const Blog = props => (
    <tr>
        <td><Link to={"/blogs/" + props.blog._id} className="text-dark"><img height="128" src={props.blog.imagesSrc[0]} alt="img" /></Link></td>
        <td><Link to={"/blogs/" + props.blog._id} className="text-dark">{props.blog.artTitle}</Link></td>
        <td>{props.blog.likesId.length}</td>
    </tr>
)

export default class BlogList extends Component {
    constructor(props) {
        super(props);

        this.onPrev = this.onPrev.bind(this);
        this.onNext = this.onNext.bind(this);
        this.onLast = this.onLast.bind(this);
        this.onFirst = this.onFirst.bind(this);

        this.state = {
            blogs: [],
            currPage: 1,
            blogsAmount: 4,
            lastPage: 1,
            loading: true
        };
    }

    componentDidMount() {
        document.title = "Blogs";

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            this.setState({ isLoggedIn: true });
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };
            axios.get(url + '/blogs', reqOptions)
                .then(response => {
                    const lp = Math.ceil(parseInt(response.data.length) / (this.state.blogsAmount));
                    this.setState({ blogs: response.data });
                    this.setState({ lastPage: lp });
                    this.setState({ loading: false });
                })
                .catch(error => this.props.history.push('/'))
        }
        else
            this.props.history.push('/unauthorized');
    }

    onPrev(e) {
        if (this.state.currPage > 1)
            this.setState({ currPage: this.state.currPage - 1 });
    }

    onNext(e) {
        if (this.state.currPage < this.state.lastPage)
            this.setState({ currPage: this.state.currPage + 1 });
    }

    onLast(e) {
        this.setState({ currPage: this.state.lastPage });
    }

    onFirst(e) {
        this.setState({ currPage: 1 });
    }

    BlogList() {
        let currPage = this.state.currPage;
        let blogsAmount = this.state.blogsAmount;
        let blogs = this.state.blogs;
        let i = (currPage - 1) * blogsAmount;

        let blogList = [];
        for (; i < (currPage - 1) * blogsAmount + blogsAmount && i < blogs.length; i++) {
            blogList.push(<Blog blog={blogs[i]} key={blogs[i]._id} />);
        }
        return blogList;
    }

    render() {
        const firstPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const lastPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const elsePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const onePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )

        let pagination;

        if (this.state.lastPage === 1)
            pagination = onePagePagination;
        else if (this.state.currPage === 1)
            pagination = firstPagePagination;
        else if (this.state.currPage === this.state.lastPage)
            pagination = lastPagePagination;
        else
            pagination = elsePagePagination

        return (
            <div className="container">
                {this.state.loading ? "Loading..."
                    :
                    <>
                        <h3>Blogs</h3>
                        <Link to="/blogs/create">
                            <button type="button" className="btn btn-dark">create new</button>
                        </Link>
                        <br/>
                        <br/>
                        {this.state.blogs.length ?
                            <>
                                <table className="table">
                                    <thead className="thead-light">
                                        <tr>
                                            <th></th>
                                            <th>Title</th>
                                            <th>Likes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.BlogList()}
                                    </tbody>
                                </table>
                                <nav aria-label="pagination">
                                    {pagination}
                                </nav>
                            </>
                            : <p id="textStyle">No Blogs</p>
                            }
                    </>
                }
            </div>
        )
    }
}