import React, { Component } from 'react';

export default class UserCreated extends Component {

    componentDidMount() {
        document.title = "Created";
    }

    render() {
        return (
            <div className="container">
                <h3>Congratulations!</h3>
                <p>Your account has been created. Please, verify your account. The messege has been sent to your E-mail</p>
            </div>
        )
    }
}