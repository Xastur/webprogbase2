import React, { Component } from 'react';
import Comment from "./comment.component";

export default class CommentList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            commentsId: this.props.commentsId,
            blogId: this.props.blogId
        }
    }

    componentDidMount() {

        const jwt = localStorage.getItem('jwt');

        if (!jwt) 
            this.props.history.push("/unauthorized");
    }

    CommentList() {
        return this.state.commentsId.map(id => {
            return <Comment key={id} id={id} blogId={this.props.blogId} />
        })
    }

    render() {

        return (
            <div className="commentList">
                <h5 className="text-muted mb-4">
                    <span className="badge badge-success">{this.state.commentsId.length}</span>{" "}
                    Comment{this.state.commentsId.length > 0 ? "s" : ""}
                </h5>

                {this.state.commentsId.length === 0 && !this.props.loading ? (
                    <div className="alert text-center alert-info">
                        Be the first to comment
              </div>
                ) : null}

                {this.CommentList()}
            </div>
        )
    }
}