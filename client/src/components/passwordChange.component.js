import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

export default class PasswordChange extends Component {
    constructor(props) {
        super(props);

        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangePasswordConfirm = this.onChangePasswordConfirm.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            password: '',
            passwordConfirm: '',
            message: '',
            token: '',
            loading: true
        }
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        })
    }

    onChangePasswordConfirm(e) {
        this.setState({
            passwordConfirm: e.target.value
        })
    }

    componentDidMount() {
        document.title = "Change Password";

        if (this.props.match.params.token) {
            this.setState({ token: this.props.match.params.token });

            axios.get(url + '/users/token/' + this.props.match.params.token)
                .then(user => {
                    console.log(user);
                    if (user.data) {
                        this.setState({ loading: false });
                    }
                    else
                        this.props.history.push('/auth/passwordreset');

                })
                .catch(err => {
                    console.log(err.message);
                    this.props.history.push('/auth/passwordreset');
                })
        }
        else
            this.props.history.push('/auth/passwordreset');
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.password === this.state.passwordConfirm) {
            if (this.state.password.length < 5) {
                this.setState({ message: "Password is too short" });
                this.setState({ password: '' });
                this.setState({ passwordConfirm: '' });
            }
            else {
                let fd = new FormData();
                fd.append('password', this.state.password);
                axios.post(url + '/auth/password/change/' + this.state.token, fd)
                    .then(x => {
                        console.log(x);
                        this.props.history.push('/auth/login');
                    })
                    .catch(err => {
                        console.log(err.message);
                    })
            }
        }
        else {
            this.setState({ message: "Passwords not match" });
            this.setState({ password: '' });
            this.setState({ passwordConfirm: '' });
        }
    }

    render() {

        return (
            <div className="container">
                {this.state.loading ?
                    "Loading..."
                    :
                    <>
                        <div className="form-group">
                            <h3>Password Change</h3>
                        </div>

                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label>New Password: </label>
                                <input type="password"
                                    required
                                    className="form-control"
                                    value={this.state.password}
                                    onChange={this.onChangePassword}
                                />
                            </div>
                            <div className="form-group">
                                <label>Confirm New Password: </label>
                                <input type="password"
                                    required
                                    className="form-control"
                                    value={this.state.passwordConfirm}
                                    onChange={this.onChangePasswordConfirm}
                                />
                            </div>
                            <div className="form-group">
                                <p>{this.state.message}</p>
                            </div>
                            <div className="form-group">
                                <input type="submit" value="Save" className="btn btn-dark" />
                            </div>
                        </form>
                    </>
                }

            </div>
        )
    }
}