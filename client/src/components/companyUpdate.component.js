import React, { Component } from 'react';
import axios from 'axios';
import config from '../config/config';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
const url = config.url;

export default class BlogUpdate extends Component {

    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeCompanyName = this.onChangeCompanyName.bind(this);
        this.onChangeAboutCompany = this.onChangeAboutCompany.bind(this);
        this.onChangeSelectedFile = this.onChangeSelectedFile.bind(this);
        this.onChangeOpenModal = this.onChangeOpenModal.bind(this);
        this.onDelete = this.onDelete.bind(this);

        this.state = {
            _id: '',
            companyName: '',
            aboutCompany: '',
            selectedFile: '',
            openModal: false,
            message: '',
            saveBtnDisabled: false,
            deleteBtnDisabled: false,
            admin: false,
            currUserId: '',
            loading: true
        }
    }

    componentDidMount() {
        document.title = "Update Company";

        const jwt = localStorage.getItem('jwt');
        if (!jwt) {
            this.props.history.push("/unauthorized");
        }
        else {
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };
            axios.get(url + '/users/me', reqOptions)
                .then(user => {
                    if (user.data.user.role) 
                        this.setState({ admin: true });
                        
                    this.setState({ currUserId: user.data.user._id });
                    return axios.get(url + '/companies/' + this.props.match.params.id, reqOptions);
                })
                .then(company => {
                    if (!this.state.admin) {
                        for (let i = 0; i < company.data.admins.length; i++) {
                            if (company.data.admins[i] === this.state.currUserId) {
                                this.setState({ admin: true });
                                break;
                            }
                            if (i === company.data.admins.length - 1) {
                                this.props.history.push("/forbidden");
                                return;
                            }
                        }
                    }
                    this.setState({ _id: company.data._id });
                    this.setState({ companyName: company.data.companyName });
                    this.setState({ aboutCompany: company.data.aboutCompany });
                    this.setState({ loading: false });
                })
                .catch(err => {
                    console.log(err.message);
                    this.props.history.push("/");
                })
        }
    }

    onSubmit(e) {
        e.preventDefault();

        const jwt = localStorage.getItem('jwt');

        if (jwt) {
            this.setState({ saveBtnDisabled: true });
            this.setState({ deleteBtnDisabled: true });

            const fd = new FormData();

            if (this.state.selectedFile)
                fd.append('image', this.state.selectedFile, this.state.selectedFile.name)

            fd.append('companyName', this.state.companyName)
            fd.append('aboutCompany', this.state.aboutCompany)

            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            axios.put(url + '/companies/' + this.state._id + '/update', fd, reqOptions)
                .then(x => {
                    this.props.history.push('/companies/' + this.state._id);
                })
                .catch(err => {
                    console.log(err.message);
                    this.setState({ message: err.message });
                    this.setState({ saveBtnDisabled: false });
                    this.setState({ deleteBtnDisabled: false });
                })
        }
        else {
            this.props.history.push("/unauthorized");
        }
    }

    onChangeCompanyName(e) {
        this.setState({ message: '' });
        this.setState({ companyName: e.target.value })
    }

    onChangeAboutCompany(e) {
        this.setState({ message: '' });
        this.setState({ aboutCompany: e.target.value })
    }

    onChangeSelectedFile(e) {
        this.setState({ message: '' });
        this.setState({ selectedFile: e.target.files[0] })
    }

    onDelete(e) {
        this.setState({ openModal: false })
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            axios.delete(url + '/companies/' + this.state._id + '/delete', reqOptions)
                .then(x => {
                    this.props.history.push("/companies");
                })
                .catch(err => {
                    console.log(err);
                    this.props.history.push("/notfound");
                });
        }
        else
            this.props.history.push("/unauthorized");
    }

    onChangeOpenModal(e) {
        if (this.state.openModal)
            this.setState({ openModal: false })
        else
            this.setState({ openModal: true })
    }

    render() {

        let deleteBtn = (
            <input type="submit"
                className="btn btn-danger"
                value="Delete company"
                onClick={this.onChangeOpenModal}
                data-toggle="modal"
                data-target="#basicExampleModal"
            />
        )
        if (this.state.deleteBtnDisabled) {
            deleteBtn = (
                <input type="submit"
                    className="btn btn-danger"
                    value="Delete company"
                    onClick={this.onChangeOpenModal}
                    data-toggle="modal"
                    data-target="#basicExampleModal"
                    disabled
                />
            )
        }
        let saveBnt = (
            <div className="form-group">
                <input type="submit"
                    value="Save"
                    className="btn btn-success"
                />
            </div>
        )
        if (this.state.saveBtnDisabled) {
            saveBnt = (
                <div className="form-group">
                    <input type="submit"
                        value="Save"
                        className="btn btn-success"
                        disabled
                    />
                </div>
            )
        }

        return (
            <div className="container">
                {this.state.loading ? "Loading..."
                    :
                    <>
                        <h3>Update Company</h3>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label id="textStyle">Company Name *</label>
                                <input type="text"
                                    required
                                    className="form-control"
                                    value={this.state.companyName}
                                    onChange={this.onChangeCompanyName}
                                    id="textStyle"
                                />
                                <br />
                                <label id="textStyle">About Company: </label>
                                <textarea className="form-control"
                                    value={this.state.aboutCompany}
                                    onChange={this.onChangeAboutCompany}
                                    rows="8" cols="50"
                                    id="textStyle"
                                />
                                <p id="textStyle">{this.state.message}</p>
                            </div>
                            <p id="textStyle">Change image:</p>
                            <input className="btn btn-dark" type="file" name="selectedFile" onChange={this.onChangeSelectedFile} />
                            <br />
                            <br />
                            {saveBnt}
                        </form>

                        {deleteBtn}

                        <Modal isOpen={this.state.openModal} className={"className"}>
                            <ModalHeader>On Delete</ModalHeader>
                            <ModalBody>
                                Are you sure?
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" onClick={this.onDelete}>Confirm</Button>{' '}
                                <Button color="secondary" onClick={this.onChangeOpenModal}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                    </>
                }
            </div>
        )
    }
}