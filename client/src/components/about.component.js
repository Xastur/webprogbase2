import React, { Component } from 'react';

export default class About extends Component {

    componentDidMount() {
        document.title = "About";
    }

    render() {
        return (
            <div className="container">
                <h2>About</h2>
                <p id="textStyle">All rights reserved</p>
                <p id="textStyle"><b>Developer:</b> Лещенко Дмитро Олександрович</p>
                <p id="textStyle"><b>Our TG Bot:</b> @OutOfLag_bot</p>
            </div>
        )
    }
}