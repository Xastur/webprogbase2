import React, { Component } from 'react';
import axios from 'axios';
import config from '../config/config';
const url = config.url;
const urlClient = config.client;

export default class BlogCreate extends Component {

    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangePassportData = this.onChangePassportData.bind(this);
        this.onChangeAboutMyCar = this.onChangeAboutMyCar.bind(this);
        this.onChangeSelectedFile = this.onChangeSelectedFile.bind(this);

        this.state = {
            _id: '',
            login: '',
            artTitle: '',
            passportData: '',
            aboutMyCar: '',
            selectedFile: '',
            selectedFiles: [],
            message: '',
            disableBtn: false
        }
    }

    componentDidMount() {
        document.title = "New Blog";

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };


            axios.get(url + '/users/me', reqOptions)
                .then(user => {
                    this.setState({ _id: user.data.user._id });
                    this.setState({ login: user.data.user.login });

                    let companyIdQuery;
                    if (this.props.location.search.length > 0) {
                        if (this.props.location.search.length === 25) {
                            companyIdQuery = this.props.location.search.slice(1, 25);
                            return axios.get(url + '/companies/' + companyIdQuery, reqOptions);
                        }
                        else {
                            this.props.history.push("/");
                            return;
                        }
                    }
                    return;
                })
                .then(company => {
                    if (company) {
                        for (let i = 0; i < company.data.admins.length; i++) {
                            if (company.data.admins[i] === this.state._id) {
                                return;
                            }
                        }
                        this.props.history.push("/forbidden");
                        return;
                    }
                })
                .catch(err => {
                    this.setState({ message: "Error" });
                    console.log(err.message);
                })
        }
        else
            this.props.history.push('/unauthorized');
    }

    onSubmit(e) {
        e.preventDefault();

        const jwt = localStorage.getItem('jwt');

        if (jwt) {
            if (this.state.selectedFiles.length > 0) {

                const fd = new FormData();

                for (let i = 0; i < this.state.selectedFiles.length && i < 10; i++) {
                    fd.append('image', this.state.selectedFiles[i]);
                }

                fd.append('artTitle', this.state.artTitle);
                fd.append('aboutMyCar', this.state.aboutMyCar);
                fd.append('passportData', this.state.passportData);

                let companyIdQuery;
                if (this.props.location.search.length > 0) {
                    if (this.props.location.search.length === 25)
                        companyIdQuery = this.props.location.search.slice(1, 25);
                    else {
                        this.props.history.push("/");
                        return;
                    }
                }

                this.setState({ message: "Please, wait" });
                this.setState({ disableBtn: true });

                fd.append('companyId', companyIdQuery);

                const reqOptions = {
                    headers: {
                        Authorization: `Bearer ${jwt}`,
                    }
                };

                let blogId;
                axios.post(url + '/blogs/create', fd, reqOptions)
                    .then(blog => {
                        if (blog.data) {
                            blogId = blog.data._id;

                            let fd = new FormData();
                            fd.append('id', this.state._id);
                            fd.append('url', urlClient + '/blogs/' + blogId);
                            fd.append('artTitle', blog.data.artTitle);
                            fd.append('userLogin', this.state.login);

                            return axios.post(url + '/bot', fd, reqOptions)
                        }
                    })
                    .then(x => {
                        if (x.data) {
                            let fd = new FormData();
                            fd.append('blogId', blogId);
                            return axios.put(url + '/users/update/subBlogsId/add', fd, reqOptions);
                        }
                    })
                    .then(x => {
                        if (x.data)
                            this.props.history.push('/blogs/' + blogId);
                    })
                    .catch(err => {
                        this.setState({
                            message: "Error"
                        })
                        console.log(err.message);
                        this.setState({ disableBtn: false });
                    })
            }
            else {
                this.setState({
                    message: "Photo is required"
                })
            }
        }
        else {
            this.props.history.push("/unauthorized");
        }
    }

    onChangeTitle(e) {
        this.setState({ message: '' });
        this.setState({ artTitle: e.target.value });
    }

    onChangePassportData(e) {
        this.setState({ message: '' });
        this.setState({ passportData: e.target.value });
    }

    onChangeAboutMyCar(e) {
        this.setState({ message: '' });
        this.setState({ aboutMyCar: e.target.value });
    }

    onChangeSelectedFile(e) {
        this.setState({ message: '' });
        this.setState({ selectedFiles: e.target.files });
    }

    render() {
        let saveBtn = (
            <input type="submit"
                value="Save"
                className="btn btn-success"
            />
        )
        if (this.state.disableBtn) {
            saveBtn = (
                <input type="submit"
                    value="Save"
                    className="btn btn-success"
                    disabled
            />   )
        }

        return (
            <div className="container">
                <h3>Create Blog</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label id="textStyle">Title: *</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.artTitle}
                            onChange={this.onChangeTitle}
                            id="textStyle"
                        />
                        <br />
                        <label id="textStyle">About My Car: </label>
                        <textarea className="form-control"
                            value={this.state.aboutMyCar}
                            onChange={this.onChangeAboutMyCar}
                            rows="8" cols="50"
                            id="textStyle"
                        />
                        <br />
                        <label id="textStyle">Passport data: </label>
                        <textarea className="form-control"
                            value={this.state.passportData}
                            onChange={this.onChangePassportData}
                            rows="5" cols="50"
                            id="textStyle"
                        />
                        <p>{this.state.message}</p>
                    </div>
                    <label id="textStyle">Choose image: *</label><br />
                    <input className="btn btn-dark" type="file" name="selectedFile" onChange={this.onChangeSelectedFile} multiple /><br /><br />
                    <div className="form-group">
                        {saveBtn}
                    </div>
                </form>
            </div>
        )
    }
}