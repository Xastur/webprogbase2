import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

const Company = props => (
    <tr>
        <td><Link to={"/companies/" + props.company._id} className="text-dark"><img height="128" src={props.company.imageSrc} alt="img" /></Link></td>
        <td><Link to={"/companies/" + props.company._id} className="text-dark">{props.company.companyName}</Link></td>
    </tr>
)

export default class MyCompaniesList extends Component {
    constructor(props) {
        super(props);

        this.onPrev = this.onPrev.bind(this);
        this.onNext = this.onNext.bind(this);
        this.onLast = this.onLast.bind(this);
        this.onFirst = this.onFirst.bind(this);

        this.state = {
            currUserId: '',
            companies: [],
            companiesId: [],
            currPage: 1,
            companiesAmount: 4,
            lastPage: 1,
            owner: false,
            loading: true
        };
    }

    componentDidMount() {
        document.title = "My Companies";

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            this.setState({ isLoggedIn: true });
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };

            axios.get(url + '/users/' + this.props.match.params.id, reqOptions)
                .then(user => {
                    if (user.data) {
                        this.setState({ companiesId: user.data.companiesId });
                        return axios.get(url + '/companies', reqOptions);
                    }
                    else {
                        this.props.history.push('/notfound');
                        return;
                    }
                })
                .then(companies => {

                    if (this.state.companiesId.length > 0) {
                        for (let i = 0; i < companies.data.length; i++) {
                            for (let j = 0; j < this.state.companiesId.length; j++) {
                                if (companies.data[i]._id === this.state.companiesId[j])
                                    this.setState({ companies: this.state.companies.concat(companies.data[i]) });
                            }
                        }
                        const lp = Math.ceil(parseInt(this.state.companies.length) / (this.state.companiesAmount));
                        this.setState({ lastPage: lp });
                    }
                    else {
                        this.setState({ lastPage: 0 });
                    }

                    return axios.get(url + '/users/me', reqOptions);
                })
                .then(user => {
                    if (user.data.user._id === this.props.match.params.id)
                        this.setState({ owner: true });

                    this.setState({ loading: false });
                })
                .catch(err => console.log(err))
        }
        else
            this.props.history.push("/unauthorized");
    }

    onPrev(e) {
        if (this.state.currPage > 1)
            this.setState({ currPage: this.state.currPage - 1 });
    }

    onNext(e) {
        if (this.state.currPage < this.state.lastPage)
            this.setState({ currPage: this.state.currPage + 1 });
    }

    onLast(e) {
        this.setState({ currPage: this.state.lastPage });
    }

    onFirst(e) {
        this.setState({ currPage: 1 });
    }

    CompanyList() {
        let currPage = this.state.currPage;
        let companiesAmount = this.state.companiesAmount;
        let companies = this.state.companies;
        let i = (currPage - 1) * companiesAmount;

        let companiesList = [];
        for (; i < (currPage - 1) * companiesAmount + companiesAmount && i < companies.length; i++) {
            companiesList.push(<Company company={companies[i]} key={companies[i]._id} />);
        }
        return companiesList;
    }

    render() {
        const firstPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const lastPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const elsePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const onePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )

        let pagination;

        if (this.state.lastPage === 1)
            pagination = onePagePagination;
        else if (this.state.currPage === 1)
            pagination = firstPagePagination;
        else if (this.state.currPage === this.state.lastPage)
            pagination = lastPagePagination;
        else
            pagination = elsePagePagination

        return (
            <div className="container">
                {this.state.loading ? "Loading..."
                    :
                    <>
                        <h3>My Companies</h3>
                        {this.state.owner ?
                            <>
                                <Link to="/companies/create">
                                    <button type="button" className="btn btn-dark">create new</button>
                                </Link>
                            </>
                            : " "
                        }
                        <br />
                        <br />
                        {this.state.lastPage ?
                            <>
                                <table className="table">
                                    <thead className="thead-light">
                                        <tr>
                                            <th></th>
                                            <th>Company Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.CompanyList()}
                                    </tbody>
                                </table>
                                <nav aria-label="pagination">
                                    {pagination}
                                </nav>
                            </>
                            : <p id="textStyle">No Companies</p>
                        }
                    </>
                }
            </div>
        )
    }
}