import React, { Component } from 'react';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.onChangeLogin = this.onChangeLogin.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeFullname = this.onChangeFullname.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeConfirmPassword = this.onChangeConfirmPassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            login: '',
            fullname: '',
            email: '',
            password: '',
            confirmPassword: '',
            message: '',
            signUpBtnDisabled: false
        }
    }

    componentDidMount() {
        document.title = "Sign Up";
    }

    onChangeLogin(e) {
        this.setState({ message: '' });
        this.setState({ login: e.target.value });
    }

    onChangeEmail(e) {
        this.setState({ message: '' });
        this.setState({ email: e.target.value });
    }

    onChangeFullname(e) {
        this.setState({ message: '' });
        this.setState({ fullname: e.target.value });
    }

    onChangeConfirmPassword(e) {
        this.setState({ message: '' });
        this.setState({ confirmPassword: e.target.value });
    }

    onChangePassword(e) {
        this.setState({ message: '' });
        this.setState({ password: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.password !== this.state.confirmPassword) {
            this.setState({ message: "Passwords not match" });
            this.setState({ password: '' });
            this.setState({ confirmPassword: '' });
            return;
        }
        else if (this.state.password.length < 5) {
            this.setState({ message: "Password is too short (less than 5 chars)" });
            this.setState({ password: '' });
            this.setState({ confirmPassword: '' });
            return;
        }

        
        const user = {
            login: this.state.login,
            fullname: this.state.fullname,
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword
        }
        
        this.setState({ signUpBtnDisabled: true });
        this.setState({ message: "Please, wait" });
        this.setState({ password: '' });
        this.setState({ confirmPassword: '' });

        axios.post(url + '/auth/signup', user)
            .then(authResult => {
                if (authResult.data.message) {
                    this.setState({
                        message: authResult.data.message,
                        confirmPassword: '',
                        signUpBtnDisabled: false
                    })
                    return;
                }
                this.props.history.push("/users/created");
            })
            .catch(err => {
                console.log(err.message);
                this.setState({ message: "Wrong login or password" })
                this.setState({ signUpBtnDisabled: false });
            })

        this.setState({
            username: '',
            password: ''
        })
    }

    render() {

        let signUpBtn = (
            <div className="form-group">
                <input type="submit"
                    value="Sign Up"
                    className="btn btn-dark"
                />
            </div>
        )
        if (this.state.signUpBtnDisabled) {
            signUpBtn = (
                <div className="form-group">
                    <input type="submit"
                        value="Sign Up"
                        className="btn btn-dark"
                        disabled
                    />
                </div>
            )
        }

        return (
            <div className="container">
                <div className="form-group">
                    <h3>Sign Up</h3>
                </div>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Login: *</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.login}
                            onChange={this.onChangeLogin}
                        />
                    </div>
                    <div className="form-group">
                        <label>Fullname: *</label>
                        <input type="text"
                            required
                            className="form-control"
                            value={this.state.fullname}
                            onChange={this.onChangeFullname}
                        />
                    </div>
                    <div className="form-group">
                        <label>E-mail: *</label>
                        <input type="email"
                            required
                            className="form-control"
                            value={this.state.email}
                            onChange={this.onChangeEmail}
                        />
                    </div>
                    <div className="form-group">
                        <label>Password: *</label>
                        <input type="password"
                            required
                            className="form-control"
                            value={this.state.password}
                            onChange={this.onChangePassword}
                        />
                    </div>
                    <div className="form-group">
                        <label>Confirm Password: *</label>
                        <input type="password"
                            required
                            className="form-control"
                            value={this.state.confirmPassword}
                            onChange={this.onChangeConfirmPassword}
                        />
                    </div>
                    <div className="form-group">
                        <p>{this.state.message}</p>
                    </div>
                    {signUpBtn}
                </form>
            </div>
        )
    }
}