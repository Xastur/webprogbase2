import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

function refreshPage() {
    window.location.reload(false);
}

class Blog extends Component {
    constructor(props) {
        super(props);

        this.onDelete = this.onDelete.bind(this);

        this.state = {}
    }

    onDelete(e) {
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            this.setState({ isLoggedIn: true });
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };
            let fd = new FormData();
            fd.append('subBlogId', this.props.blog._id);

            axios.get(url + '/users/me', reqOptions)
                .then(user => {
                    return axios.put(url + '/users/' + user.data.user._id + '/update', fd, reqOptions);
                })
                .then(x => {
                    refreshPage();
                })
                .catch(err => console.log(err.message))
        }
        else
            this.props.history.push("/unauthorized");
    }

    render() {

        return (
            <tr>
                <td><Link to={"/blogs/" + this.props.blog._id} className="text-dark"><img height="128" src={this.props.blog.imagesSrc[0]} alt="img" /></Link></td>
                <td><Link to={"/blogs/" + this.props.blog._id} className="text-dark">{this.props.blog.artTitle}</Link></td>
                <td id="deleteTextId" onClick={this.onDelete}>delete</td>
            </tr>
        )
    }
}

export default class News extends Component {
    constructor(props) {
        super(props);

        this.onPrev = this.onPrev.bind(this);
        this.onNext = this.onNext.bind(this);
        this.onLast = this.onLast.bind(this);
        this.onFirst = this.onFirst.bind(this);

        this.state = {
            currUserId: '',
            blogs: [],
            blogsId: [],
            currPage: 1,
            blogsAmount: 4,
            lastPage: 1,
            loading: true
        };
    }

    componentDidMount() {
        document.title = "News";

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            this.setState({ isLoggedIn: true });
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };

            axios.get(url + '/users/me', reqOptions)
                .then(user => {
                    this.setState({ blogsId: user.data.user.subBlogsId });
                    return axios.get(url + '/blogs', reqOptions);
                })
                .then(blogs => {

                    if (this.state.blogsId.length > 0) {
                        for (let i = 0; i < blogs.data.length; i++) {
                            for (let j = 0; j < this.state.blogsId.length; j++) {
                                if (blogs.data[i]._id === this.state.blogsId[j])
                                    this.setState({ blogs: this.state.blogs.concat(blogs.data[i]) });
                            }
                        }
                        const lp = Math.ceil(parseInt(this.state.blogs.length) / (this.state.blogsAmount));
                        this.setState({ lastPage: lp });
                    }
                    else {
                        this.setState({ lastPage: 0 });
                    }
                    this.setState({ loading: false });
                })
                .catch(err => console.log(err))
        }
        else
            this.props.history.push('/');
    }

    onPrev(e) {
        if (this.state.currPage > 1)
            this.setState({ currPage: this.state.currPage - 1 });
    }

    onNext(e) {
        if (this.state.currPage < this.state.lastPage)
            this.setState({ currPage: this.state.currPage + 1 });
    }

    onLast(e) {
        this.setState({ currPage: this.state.lastPage });
    }

    onFirst(e) {
        this.setState({ currPage: 1 });
    }

    BlogList() {
        let currPage = this.state.currPage;
        let blogsAmount = this.state.blogsAmount;
        let blogs = this.state.blogs;

        let blogList = [];
        let i = blogs.length - (currPage - 1) * blogsAmount - 1;
        for (; i >= 0 && i > blogs.length - (currPage - 1) * blogsAmount - blogsAmount - 1; i--) {
            blogList.push(<Blog blog={blogs[i]} key={blogs[i]._id} />);
        }
        return blogList;
    }

    render() {
        const firstPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const lastPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const elsePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const onePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )

        let pagination;

        if (this.state.lastPage === 1)
            pagination = onePagePagination;
        else if (this.state.currPage === 1)
            pagination = firstPagePagination;
        else if (this.state.currPage === this.state.lastPage)
            pagination = lastPagePagination;
        else
            pagination = elsePagePagination

        return (
            <div className="container">
                {this.state.loading ? "Loading..."
                    :
                    <>
                        <h3>News</h3>
                        <br />
                        <br />
                        {this.state.lastPage ?
                            <>
                                <table className="table">
                                    <thead className="thead-light">
                                        <tr>
                                            <th></th>
                                            <th>Title</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.BlogList()}
                                    </tbody>
                                </table>
                                <nav aria-label="pagination">
                                    {pagination}
                                </nav>
                            </>
                            : <p id="textStyle">No News</p>
                        }
                    </>
                }
            </div>
        )
    }
}