import React, { Component } from 'react';
import axios from 'axios';
import config from '../config/config';
import { Link } from 'react-router-dom';
import dateFormat from 'dateformat';
const url = config.url;


export default class User extends Component {

    constructor(props) {
        super(props);

        // this.onLogout = this.onLogout.bind(this);
        this.onSubscribe = this.onSubscribe.bind(this);

        this.state = {
            _id: '', //mongoose.Schema.Types.ObjectId,
            currUser: '',
            login: '', //String,
            fullname: '',// String,
            biography: '',//String,
            registeredAt: '',//Date,
            avaUrl: '',//String,
            articlesId: '',//Array,
            companiesId: '',//Array,
            subscribersId: '',
            owner: false,
            admin: false,
            subscribeBtnText: 'Subscribe',
            tgToken: 'Not yours',
            loaded: false,
            email: ''
        }
    }

    handleClick = e => {
        this.props.history.push("/users/" + this.state._id + "/update");
    }

    // onLogout(e) {
    //     localStorage.removeItem('jwt');
    //     this.props.history.push("/");
    //     window.location.reload(false);
    // }

    onSubscribe(e) {
        if (this.state.subscribeBtnText === 'Subscribe')
            this.setState({ subscribeBtnText: "Unsubscribe" });
        else
            this.setState({ subscribeBtnText: "Subscribe" });

        const jwt = localStorage.getItem('jwt');
        if (jwt && !this.state.owner) {
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };

            let fd = new FormData();
            fd.append('subscriptionId', this.state._id);

            axios.put(url + '/users/' + this.state.currUser._id + '/update', fd, reqOptions)
                .then(x => {
                    let fd1 = new FormData();
                    fd1.append('subscriberId', this.state.currUser._id);
                    return axios.put(url + '/users/' + this.state._id + '/update', fd1, reqOptions);
                })
                .then(x => {
                    console.log("Subscribed or unsubscribed!");
                })
                .catch(err => console.log(err.message));
        }
        else
            this.props.history.push("/");
    }
    componentDidMount() {
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };
            axios.get(url + '/users/' + this.props.match.params.id, reqOptions)
                .then(x => {
                    if (!x.data) {
                        this.props.history.push("/notfound");
                        return;
                    }
                    this.setState({ _id: x.data._id });
                    this.setState({ avaUrl: x.data.avaUrl });
                    this.setState({ fullname: x.data.fullname });
                    this.setState({ login: x.data.login });
                    this.setState({ registeredAt: x.data.registeredAt });
                    this.setState({ biography: x.data.biography });
                    this.setState({ subscribersId: x.data.subscribersId });
                    this.setState({ email: x.data.email });

                    document.title = x.data.login;

                    return axios.get(url + "/users/me", reqOptions);
                })
                .then(x => {
                    if (x.data) {
                        this.setState({ currUser: x.data.user });
                        if (x.data.user._id === this.state._id) {
                            this.setState({ owner: true });
                            // this.setState({ admin: true });
                            this.setState({ tgToken: x.data.user.tgToken });
                        }
                        else if (x.data.user.role)
                            this.setState({ admin: true });

                        if (x.data.user._id !== this.state._id) {
                            for (let i = 0; i < this.state.subscribersId.length; i++) {
                                if (this.state.subscribersId[i] === this.state.currUser._id) {
                                    console.log("You are subscriber");
                                    this.setState({ subscribeBtnText: 'Unsubscribe' });
                                    this.setState({ loaded: true });
                                    return;
                                }
                            }
                            console.log("You are not a subscriber");
                            this.setState({ subscribeBtnText: 'Subscribe' });
                        }
                        this.setState({ loaded: true });
                    }
                })
                .catch(err => this.props.history.push("/notfound"))
        }
        else
            this.props.history.push("/unauthorized");
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.match.params.id && prevState._id) {
            if (nextProps.match.params.id !== prevState._id) {
                window.location.reload(false);
            }
        }
        return null;
    }

    render() {
        const settingsBtn = (
            <input type="submit"
                className="btn btn-dark"
                value="Settings"
                onClick={this.handleClick}
                id="myPageBtnsId"
            />
        )
        const blogsBtn = (
            <input type="submit"
                className="btn btn-dark"
                value="My Blogs"
                id="myBlogsId"
            />
        )
        const companiesBtn = (
            <input type="submit"
                className="btn btn-dark"
                value="My Companies"
                id="myPageBtnsId"
            />
        )
        const subscribeBtn = (
            <input type="submit"
                className="btn btn-dark"
                value={this.state.subscribeBtnText}
                onClick={this.onSubscribe}
                id="myPageBtnsId"
            />
        )
        const subscribersBtn = (
            <input type="submit"
                className="btn btn-dark"
                value="Subscribers"
                id="myPageBtnsId"
            />
        )
        const subscriptionsBtn = (
            <input type="submit"
                className="btn btn-dark"
                value="Subscriptions"
                id="myPageBtnsId"
            />
        )

        const myBLogsUrl = '/users/' + this.state._id + '/blogs';
        const myCompaniesUrl = '/users/' + this.state._id + '/companies';
        const subscribersUrl = '/users/' + this.state._id + '/subscribers';
        const subscriptionsUrl = '/users/' + this.state._id + '/subscriptions';

        return (
            < div className="container" >
                {this.state.loaded ?
                    <>
                        <img id="ava" src={this.state.avaUrl} alt="img" />
                        <h5 id="info">Fullname: {this.state.fullname}</h5>
                        <h5 id="info">E-mail: {this.state.email}</h5>
                        {/* <br/> */}
                        <h5 id="info">Login: {this.state.login}</h5>
                        <h5 id="info">Registered at: {dateFormat(this.state.registeredAt, "mmmm dS, yyyy")}</h5>
                        <hr />

                        {this.state.biography.length ?
                            <>
                                <h5 id="info">About me:</h5>
                                <p id="textStyle">{this.state.biography}</p>
                            </>
                            : ""}

                        <Link to={myBLogsUrl}>
                            {blogsBtn}
                        </Link>
                        <Link to={myCompaniesUrl}>
                            {companiesBtn}
                        </Link>
                        <Link to={subscribersUrl}>
                            {subscribersBtn}
                        </Link>
                        <Link to={subscriptionsUrl}>
                            {subscriptionsBtn}

                        </Link>
                        {this.state.owner ? '' : subscribeBtn}
                        {this.state.admin ? settingsBtn : ''}
                    </>
                    : "Loading..."
                }
            </div>
        )
    }
}