import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
const url = config.url;

const Blog = props => (
    <tr>
        <td><Link to={"/blogs/" + props.blog._id} className="text-dark"><img height="128" src={props.blog.imagesSrc[0]} alt="img" /></Link></td>
        <td><Link to={"/blogs/" + props.blog._id} className="text-dark">{props.blog.artTitle}</Link></td>
        <td>{props.blog.likesId.length}</td>
    </tr>
)

export default class CompanyBLogs extends Component {
    constructor(props) {
        super(props);

        this.onPrev = this.onPrev.bind(this);
        this.onNext = this.onNext.bind(this);
        this.onLast = this.onLast.bind(this);
        this.onFirst = this.onFirst.bind(this);

        this.state = {
            blogsId: [],
            blogs: [],
            currPage: 1,
            blogsAmount: 4,
            lastPage: 1,
            adminsId: [],
            isAdmin: false,
            loading: true
        };
    }

    componentDidMount() {
        document.title = "Blogs";

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            this.setState({ isLoggedIn: true });
            const reqOptions = {
                headers: { Authorization: `Bearer ${jwt}`, },
            };

            axios.get(url + '/companies/' + this.props.match.params.id, reqOptions)
                .then(company => {
                    if (!company.data) {
                        this.props.history.push('/notfound');
                        return;
                    }
                    this.setState({ blogsId: company.data.articlesId });
                    this.setState({ adminsId: company.data.admins });

                    return axios.get(url + '/blogs', reqOptions);
                })
                .then(blogs => {
                    if (blogs.data) {
                        blogs.data.forEach(blog => {
                            for (let i = 0; i < this.state.blogsId.length; i++) {
                                if (blog._id === this.state.blogsId[i]) {
                                    this.setState({ blogs: this.state.blogs.concat(blog) });
                                }
                            }
                        })
                        const lp = Math.ceil(parseInt(this.state.blogs.length) / (this.state.blogsAmount));
                        this.setState({ lastPage: lp });

                        return axios.get(url + '/users/me', reqOptions);
                    }
                    return;
                })
                .then(user => {
                    if (user.data) {
                        for (let i = 0; i < this.state.adminsId.length; i++) {
                            if (user.data.user._id === this.state.adminsId[i]) {
                                this.setState({ isAdmin: true });
                                break;
                            }
                        }
                        this.setState({ loading: false });
                    }
                })
                .catch(err => console.log(err))
        }
        else
            this.props.history.push("/unauthorized");
    }

    onPrev(e) {
        if (this.state.currPage > 1)
            this.setState({ currPage: this.state.currPage - 1 });
    }

    onNext(e) {
        if (this.state.currPage < this.state.lastPage)
            this.setState({ currPage: this.state.currPage + 1 });
    }

    onLast(e) {
        this.setState({ currPage: this.state.lastPage });
    }

    onFirst(e) {
        this.setState({ currPage: 1 });
    }

    BlogList() {
        let currPage = this.state.currPage;
        let blogsAmount = this.state.blogsAmount;
        let blogs = this.state.blogs;
        let i = (currPage - 1) * blogsAmount;

        let blogList = [];
        for (; i < (currPage - 1) * blogsAmount + blogsAmount && i < blogs.length; i++) {
            blogList.push(<Blog blog={blogs[i]} key={blogs[i]._id} />);
        }
        return blogList;
    }

    render() {
        const firstPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const lastPagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const elsePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item" onClick={this.onFirst}><p className="page-link">1</p></li>
                <li>...</li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li>...</li>
                <li className="page-item" onClick={this.onLast}><p className="page-link">{this.state.lastPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )
        const onePagePagination = (
            <ul className="pagination">
                <li className="page-item" onClick={this.onPrev}><p className="page-link">Previous</p></li>
                <li className="page-item active"><p className="page-link">{this.state.currPage}</p></li>
                <li className="page-item" onClick={this.onNext}><p className="page-link">Next</p></li>
            </ul>
        )

        let pagination;

        if (this.state.lastPage === 1)
            pagination = onePagePagination;
        else if (this.state.currPage === 1)
            pagination = firstPagePagination;
        else if (this.state.currPage === this.state.lastPage)
            pagination = lastPagePagination;
        else
            pagination = elsePagePagination

        const blogCreateUrl = '/blogs/create?' + this.props.match.params.id;

        return (
            <div className="container">
                {this.state.loading ? "Loading..."
                    :
                    <>
                        <h3>Our Blogs</h3>
                        {this.state.isAdmin ?
                            <>
                                <Link to={blogCreateUrl}>
                                    <button type="button" className="btn btn-dark">create new</button>
                                </Link>
                            </>
                            : ""
                        }
                        {this.state.lastPage ?
                            <>
                                <table className="table">
                                    <thead className="thead-light">
                                        <tr>
                                            <th></th>
                                            <th>Title</th>
                                            <th>Likes amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.BlogList()}
                                    </tbody>
                                </table>
                                <nav aria-label="pagination">
                                    {pagination}
                                </nav>
                            </>
                            : <h5><br />We have no blogs :(</h5>}
                    </>
                }
            </div>
        )
    }
}