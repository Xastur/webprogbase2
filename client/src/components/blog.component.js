import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';
import CommentList from "./comment-list.component";
import Carousel from 'react-bootstrap/Carousel'
const url = config.url;

function refreshPage() {
    window.location.reload(false);
}

export default class Blog extends Component {

    constructor(props) {
        super(props);

        this.onSubmitComment = this.onSubmitComment.bind(this);
        this.onChangeComment = this.onChangeComment.bind(this);
        this.onLikeClick = this.onLikeClick.bind(this);

        this.state = {
            _id: '',
            artTitle: '',
            userId: '',
            companyId: '',
            aboutMyCar: '',
            passportData: '',
            // likesAmount: '',
            likesId: [],
            viewsAmount: '',
            date: '',
            creatorLogin: '',
            creatorId: '',
            owner: '',
            commentsId: [],
            comment: '',
            loading: true,
            photoes: [],
            currUserLiked: false
        }
    }

    onSubmitComment(e) {
        e.preventDefault();

        if (this.state.comment.length > 0) {
            const jwt = localStorage.getItem('jwt');
            if (jwt) {
                const reqOptions = {
                    headers: {
                        Authorization: `Bearer ${jwt}`,
                    }
                };

                let fd = new FormData();
                fd.append('comment', this.state.comment);
                fd.append('_id', this.state._id);

                axios.post(url + '/comments/create', fd, reqOptions)
                    .then(x => refreshPage())
                    .catch(err => console.log(err))
            }
            else
                this.props.history.push("/unauthorized");
        }
    }

    onChangeComment(e) {
        this.setState({ comment: e.target.value });
    }

    onLikeClick(e) {

        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            if (this.state.currUserLiked)
                this.setState({ currUserLiked: false });
            else
                this.setState({ currUserLiked: true });

            let fd = new FormData();
            fd.append('userId', this.state.userId);

            axios.put(url + '/blogs/' + this.state._id + '/like', fd, reqOptions)
                .then(x => {
                    this.setState({ likesAmount: x.data.likesId.length });
                })
                .catch(err => {
                    console.log(err.message);
                })
        }
    }

    componentDidMount() {
        const jwt = localStorage.getItem('jwt');
        if (jwt) {
            const reqOptions = {
                headers: {
                    Authorization: `Bearer ${jwt}`,
                }
            };

            axios.get(url + window.location.pathname, reqOptions)
                .then(x => {
                    if (!x.data) {
                        this.props.history.push("/notfound");
                        return;
                    }
                    document.title = x.data.artTitle;

                    this.setState({ _id: x.data._id });
                    this.setState({ aboutMyCar: x.data.aboutMyCar });
                    this.setState({ artTitle: x.data.artTitle });
                    this.setState({ passportData: x.data.passportData });
                    this.setState({ commentsId: x.data.commentsId });
                    this.setState({ likesAmount: x.data.likesId.length });
                    this.setState({ photoes: x.data.imagesSrc });
                    this.setState({ likesId: x.data.likesId });

                    return axios.get(url + '/users/' + x.data.userId, reqOptions);
                })
                .then(user => {
                    if (user.data) {
                        this.setState({ creatorId: user.data._id });
                        this.setState({ creatorLogin: user.data.login });

                        return axios.get(url + '/users/me', reqOptions);
                    }
                    return;
                })
                .then(user => {
                    if (user.data) {
                        if (user.data.user._id === this.state.creatorId || user.data.user.role)
                            this.setState({ owner: true });

                        for (let i = 0; i < this.state.likesId.length; i++) {
                            if (this.state.likesId[i] === user.data.user._id) {
                                this.setState({ currUserLiked: true });
                                break;
                            }
                        }

                        this.setState({ userId: user.data.user._id });

                        let fd = new FormData();
                        fd.append('userId', this.state.userId);

                        return axios.put(url + '/blogs/' + this.state._id + '/view', fd, reqOptions);
                    }
                    return;
                })
                .then(x => {
                    if (x.data) {
                        this.setState({ viewsAmount: x.data.viewsId.length });
                        this.setState({ loading: false });
                    }
                })
                .catch(err => {
                    this.props.history.push("/notfound");
                })
        }
        else
            this.props.history.push("/unauthorized");
    }

    PhotoList() {
        let photoes = [];

        for (let i = 0; i < this.state.photoes.length; i++) {
            photoes.push(<Carousel.Item key={i}>
                <img
                    className="d-block w-100"
                    src={this.state.photoes[i]}
                    alt="First slide"
                />
            </Carousel.Item>);
        }
        return photoes;
    }

    render() {

        const updBtn = (
            <input type="submit"
                className="btn btn-dark"
                value="Settings"
            />
        )
        const likeBtnText = this.state.likesAmount;
        let likeBtn = (
            <button className="btn btn-light" type="submit" onClick={this.onLikeClick} id="likeBtnId">
                <img src="https://res.cloudinary.com/hhiefmflq/image/upload/v1576104484/like_bgxh1u.png" width="16" height="18" alt="like" />{likeBtnText}
            </button>
        )
        if (this.state.currUserLiked) {
            likeBtn = (
                <button className="btn btn-danger" type="submit" onClick={this.onLikeClick} id="likeBtnId">
                    <img src="https://res.cloudinary.com/hhiefmflq/image/upload/v1576104484/like_bgxh1u.png" width="16" height="18" alt="like" />{likeBtnText}
                </button>
            )
        }

        const updUrl = '/blogs/' + this.state._id + '/update';

        const viewsBtnText = this.state.viewsAmount;
        const viewsBtn = (
            <button className="btn btn-light" type="submit" id="viewsBtnId">
                <img src="https://res.cloudinary.com/hhiefmflq/image/upload/v1576104974/65000_tqx6dy.png" width="22" height="22" alt="like" />{viewsBtnText}
            </button>
        )

        const carousel = (
            <Carousel>
                {this.PhotoList()}
            </Carousel>
        )

        return (
            <div className="container">
                {this.state.loading ? "Loading..."
                    :
                    <>
                        <h1 id="artTitleId">{this.state.artTitle}</h1>

                        {likeBtn}
                        {viewsBtn}
                        <br />
                        <strong>Creator: <a className="text-dark" href={"/users/" + this.state.creatorId}>{this.state.creatorLogin}</a></strong>
                        <br />
                        <br />
                        {this.state.aboutMyCar.length ?
                            <>
                                <h5>About my car:</h5>
                                <p id="textStyle">{this.state.aboutMyCar}</p>
                            </> : ""}
                        {this.state.passportData.length ?
                            <>
                                <h5>Passport data:</h5>
                                <p id="textStyle">{this.state.passportData}</p>
                            </> : ""}

                        {carousel}
                        <Link to={updUrl}>
                            {this.state.owner ? updBtn : ''}
                        </Link>
                        <hr />
                        <br />
                        <textarea className="form-control"
                            onChange={this.onChangeComment}
                            placeholder="Leave your comment here"
                        />
                        <input type="submit"
                            className="btn btn-primary"
                            value="Submit"
                            onClick={this.onSubmitComment}
                        />
                        <br />
                        <br />
                        <div className="row">
                            <div className="col-8  pt-3 bg-white">
                                {this.state.loading ? '' : <CommentList
                                    commentsId={this.state.commentsId}
                                    blogId={this.state._id}
                                />}
                            </div>
                        </div>
                    </>
                }
            </div >
        )
    }
}