import React, { Component } from 'react';

export default class Api extends Component {

    componentDidMount() {
        document.title = "Api";
    }

    render() {
        return (
            <div className="container">
                <p id="textStyle"><strong>get /users/me:</strong> повертає JSON із інформацією про аутентифікованого користувача. Необхідна аутентифікація</p>
                <p id="textStyle"><strong>get /users:</strong> повертає список JSON користувачів. Необхідна роль адміністратора. Responds: 403, 400, 200</p>
                <p id="textStyle"><strong>get /users/:id:</strong> повертає JSON користувача по ідентифікатору. Responds: 404, 200</p>
                <p id="textStyle"><strong>put /users/:id:</strong> оновлює користувача по ідентифікатору. Оновлювати може лише власник сторінки. Responds: 403, 400, 200</p>
                <p id="textStyle"><strong>delete /users/:id:</strong> видаляє користувача по ідентифікатору, повертає res промісів. Видаляти може лише власник сторінки. Responds: 403, 400, 200</p>
                <p id="textStyle"><strong>get /blogs:</strong> повертає список JSON блогів. Необхідна аутентифікація. Responds: 403, 400, 200</p>
                <p id="textStyle"><strong>get /blogs/:id:</strong> повертає JSON блогу по ідентифікатору. Необхідна аутентифікація. Responds: 200, 404</p>
                <p id="textStyle"><strong>post /blogs:</strong> повертає JSON створеного блогу. Необхідна аутентифікація. Responds: 201, 400</p>
                <p id="textStyle"><strong>put /blogs/:id:</strong> оновлює блог по ідентифікатору. Оновлювати може лише власник блогу. Responds: 403, 400, 200</p>
                <p id="textStyle"><strong>delete /blogs/:id:</strong> видаляє блог по ідентифікатору. Responds: 403, 400, 200</p>
                <p id="textStyle"><strong>get /companies:</strong> повертає список JSON компаній. Responds: 200, 400</p>
                <p id="textStyle"><strong>get /companies/:id:</strong> повертає компанію по ідентифікатору. Responds: 200, 404</p>
                <p id="textStyle"><strong>post /companies:</strong> повертає JSON створеної компанії. Responds: 201, 400</p>
                <p id="textStyle"><strong>put /companies/:id:</strong> оноалює компанію по ідентифікатору. Responds: 200, 400</p>
                <p id="textStyle"><strong>delete /companies/:id:</strong> видаляє компанію по ідентифікатору, повертає res промісів. Responds: 403, 200, 400</p>
            </div>
        )
    }
}