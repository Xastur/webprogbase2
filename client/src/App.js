import React, { Component } from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Navbar from './components/navbar.component';
import UserList from './components/users-list.component';
import BlogList from './components/blogs-list.component';
import CompanyList from './components/companies-list.component';
import SubscribersList from './components/subscribers-list.component';
import SubscriptionsList from './components/subscriptions-list.component';
import Blog from './components/blog.component';
import Home from './components/home.component';
import Company from './components/company.component';
import About from './components/about.component';
import Login from './components/login.component';
import Signup from './components/signup.component';
import UserCreated from './components/userCreated.component';
import NotFound from './components/notfound.component';
import Forbidden from './components/forbidden.component';
import Unauthorized from './components/unauthorized.component';
import User from './components/user.component';
import UserUpdate from './components/userUpdate.component';
import BlogUpdate from './components/blogUpdate.component';
import CompanyUpdate from './components/companyUpdate.component';
import BlogCreate from './components/blogCreate.component';
import CompanyCreate from './components/companyCreate.component';
import Search from './components/search.component';
import MyBlogList from './components/myblogs-list.component';
import MyCompaniesList from './components/mycompanies-list.companent';
import CompanyAdmins from './components/companyAdmins.component';
import CompanyBlogs from './components/companyBlogs.component';
import CompanyAdminAdd from './components/companiesAdminAdd.component';
import News from './components/news.component';
import PasswordReset from './components/passwordReset.component';
import PasswordChange from './components/passwordChange.component';
import MessageSent from './components/messageSent.component';
import Api from './components/api.component';

export default class App extends Component {

  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <br />
          <Switch>
            <Route exaxt path="/users/:id/subscriptions" component={SubscriptionsList} />
            <Route exaxt path="/users/:id/subscribers" component={SubscribersList} />
            <Route exaxt path="/users/:id/update" component={UserUpdate} />
            <Route exaxt path="/users/:id/blogs" component={MyBlogList} />
            <Route exaxt path="/users/:id/companies" component={MyCompaniesList} />
            <Route exaxt path="/users/created" component={UserCreated} />
            <Route exaxt path="/users/:id" component={User} />
            <Route exaxt path="/users" component={UserList} />
            <Route exaxt path="/companies/:id/update" component={CompanyUpdate} />
            <Route exaxt path="/companies/:id/admins/add" component={CompanyAdminAdd} />
            <Route exaxt path="/companies/:id/admins" component={CompanyAdmins} />
            <Route exaxt path="/companies/:id/blogs" component={CompanyBlogs} />
            <Route exaxt path="/companies/create" component={CompanyCreate} />
            <Route exaxt path="/companies/:id" component={Company} />
            <Route exaxt path="/companies" component={CompanyList} />
            <Route exaxt path="/blogs/:id/update" component={BlogUpdate} />
            <Route exaxt path="/blogs/create" component={BlogCreate} />
            <Route exaxt path="/blogs/:id" component={Blog} />
            <Route exaxt path="/blogs" component={BlogList} />
            <Route exaxt path="/about" component={About} />
            <Route exaxt path="/auth/login" component={Login} />
            <Route exaxt path="/auth/signup" component={Signup} />
            <Route exaxt path="/auth/passwordreset" component={PasswordReset} />
            <Route exaxt path="/search/:req" component={Search} />
            <Route exaxt path="/news" component={News} />
            <Route exaxt path='/message/sent' component={MessageSent} />
            <Route exaxt path='/password/change/:token' component={PasswordChange} />
            <Route exaxt path='/notfound' component={NotFound} />
            <Route exaxt path='/forbidden' component={Forbidden} />
            <Route exaxt path='/unauthorized' component={Unauthorized} />
            <Route exaxt path="/api" component={Api} />
            <Route exaxt path="/" component={Home} />
          </Switch>
        </div>
      </Router >
    );
  }
}